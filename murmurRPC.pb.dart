///
//  Generated code. Do not modify.
//  source: murmurRPC.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

import 'dart:core' as $core;

import 'package:fixnum/fixnum.dart' as $fixnum;
import 'package:protobuf/protobuf.dart' as $pb;

import 'murmurRPC.pbenum.dart';

export 'murmurRPC.pbenum.dart';

class Void extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Void', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  Void._() : super();
  factory Void() => create();
  factory Void.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Void.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Void clone() => Void()..mergeFromMessage(this);
  Void copyWith(void Function(Void) updates) => super.copyWith((message) => updates(message as Void));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Void create() => Void._();
  Void createEmptyInstance() => create();
  static $pb.PbList<Void> createRepeated() => $pb.PbList<Void>();
  @$core.pragma('dart2js:noInline')
  static Void getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Void>(create);
  static Void _defaultInstance;
}

class Version extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Version', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..a<$core.int>(1, 'version', $pb.PbFieldType.OU3)
    ..aOS(2, 'release')
    ..aOS(3, 'os')
    ..aOS(4, 'osVersion')
    ..hasRequiredFields = false
  ;

  Version._() : super();
  factory Version() => create();
  factory Version.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Version.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Version clone() => Version()..mergeFromMessage(this);
  Version copyWith(void Function(Version) updates) => super.copyWith((message) => updates(message as Version));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Version create() => Version._();
  Version createEmptyInstance() => create();
  static $pb.PbList<Version> createRepeated() => $pb.PbList<Version>();
  @$core.pragma('dart2js:noInline')
  static Version getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Version>(create);
  static Version _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get version => $_getIZ(0);
  @$pb.TagNumber(1)
  set version($core.int v) { $_setUnsignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasVersion() => $_has(0);
  @$pb.TagNumber(1)
  void clearVersion() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get release => $_getSZ(1);
  @$pb.TagNumber(2)
  set release($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasRelease() => $_has(1);
  @$pb.TagNumber(2)
  void clearRelease() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get os => $_getSZ(2);
  @$pb.TagNumber(3)
  set os($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasOs() => $_has(2);
  @$pb.TagNumber(3)
  void clearOs() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get osVersion => $_getSZ(3);
  @$pb.TagNumber(4)
  set osVersion($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasOsVersion() => $_has(3);
  @$pb.TagNumber(4)
  void clearOsVersion() => clearField(4);
}

class Uptime extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Uptime', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..a<$fixnum.Int64>(1, 'secs', $pb.PbFieldType.OU6, defaultOrMaker: $fixnum.Int64.ZERO)
    ..hasRequiredFields = false
  ;

  Uptime._() : super();
  factory Uptime() => create();
  factory Uptime.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Uptime.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Uptime clone() => Uptime()..mergeFromMessage(this);
  Uptime copyWith(void Function(Uptime) updates) => super.copyWith((message) => updates(message as Uptime));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Uptime create() => Uptime._();
  Uptime createEmptyInstance() => create();
  static $pb.PbList<Uptime> createRepeated() => $pb.PbList<Uptime>();
  @$core.pragma('dart2js:noInline')
  static Uptime getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Uptime>(create);
  static Uptime _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get secs => $_getI64(0);
  @$pb.TagNumber(1)
  set secs($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasSecs() => $_has(0);
  @$pb.TagNumber(1)
  void clearSecs() => clearField(1);
}

class Server_Event extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Server.Event', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..aOM<Server>(1, 'server', subBuilder: Server.create)
    ..e<Server_Event_Type>(2, 'type', $pb.PbFieldType.OE, defaultOrMaker: Server_Event_Type.UserConnected, valueOf: Server_Event_Type.valueOf, enumValues: Server_Event_Type.values)
    ..aOM<User>(3, 'user', subBuilder: User.create)
    ..aOM<TextMessage>(4, 'message', subBuilder: TextMessage.create)
    ..aOM<Channel>(5, 'channel', subBuilder: Channel.create)
  ;

  Server_Event._() : super();
  factory Server_Event() => create();
  factory Server_Event.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Server_Event.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Server_Event clone() => Server_Event()..mergeFromMessage(this);
  Server_Event copyWith(void Function(Server_Event) updates) => super.copyWith((message) => updates(message as Server_Event));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Server_Event create() => Server_Event._();
  Server_Event createEmptyInstance() => create();
  static $pb.PbList<Server_Event> createRepeated() => $pb.PbList<Server_Event>();
  @$core.pragma('dart2js:noInline')
  static Server_Event getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Server_Event>(create);
  static Server_Event _defaultInstance;

  @$pb.TagNumber(1)
  Server get server => $_getN(0);
  @$pb.TagNumber(1)
  set server(Server v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasServer() => $_has(0);
  @$pb.TagNumber(1)
  void clearServer() => clearField(1);
  @$pb.TagNumber(1)
  Server ensureServer() => $_ensure(0);

  @$pb.TagNumber(2)
  Server_Event_Type get type => $_getN(1);
  @$pb.TagNumber(2)
  set type(Server_Event_Type v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasType() => $_has(1);
  @$pb.TagNumber(2)
  void clearType() => clearField(2);

  @$pb.TagNumber(3)
  User get user => $_getN(2);
  @$pb.TagNumber(3)
  set user(User v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasUser() => $_has(2);
  @$pb.TagNumber(3)
  void clearUser() => clearField(3);
  @$pb.TagNumber(3)
  User ensureUser() => $_ensure(2);

  @$pb.TagNumber(4)
  TextMessage get message => $_getN(3);
  @$pb.TagNumber(4)
  set message(TextMessage v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasMessage() => $_has(3);
  @$pb.TagNumber(4)
  void clearMessage() => clearField(4);
  @$pb.TagNumber(4)
  TextMessage ensureMessage() => $_ensure(3);

  @$pb.TagNumber(5)
  Channel get channel => $_getN(4);
  @$pb.TagNumber(5)
  set channel(Channel v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasChannel() => $_has(4);
  @$pb.TagNumber(5)
  void clearChannel() => clearField(5);
  @$pb.TagNumber(5)
  Channel ensureChannel() => $_ensure(4);
}

class Server_Query extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Server.Query', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  Server_Query._() : super();
  factory Server_Query() => create();
  factory Server_Query.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Server_Query.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Server_Query clone() => Server_Query()..mergeFromMessage(this);
  Server_Query copyWith(void Function(Server_Query) updates) => super.copyWith((message) => updates(message as Server_Query));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Server_Query create() => Server_Query._();
  Server_Query createEmptyInstance() => create();
  static $pb.PbList<Server_Query> createRepeated() => $pb.PbList<Server_Query>();
  @$core.pragma('dart2js:noInline')
  static Server_Query getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Server_Query>(create);
  static Server_Query _defaultInstance;
}

class Server_List extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Server.List', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..pc<Server>(1, 'servers', $pb.PbFieldType.PM, subBuilder: Server.create)
  ;

  Server_List._() : super();
  factory Server_List() => create();
  factory Server_List.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Server_List.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Server_List clone() => Server_List()..mergeFromMessage(this);
  Server_List copyWith(void Function(Server_List) updates) => super.copyWith((message) => updates(message as Server_List));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Server_List create() => Server_List._();
  Server_List createEmptyInstance() => create();
  static $pb.PbList<Server_List> createRepeated() => $pb.PbList<Server_List>();
  @$core.pragma('dart2js:noInline')
  static Server_List getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Server_List>(create);
  static Server_List _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<Server> get servers => $_getList(0);
}

class Server extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Server', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..a<$core.int>(1, 'id', $pb.PbFieldType.QU3)
    ..aOB(2, 'running')
    ..aOM<Uptime>(3, 'uptime', subBuilder: Uptime.create)
  ;

  Server._() : super();
  factory Server() => create();
  factory Server.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Server.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Server clone() => Server()..mergeFromMessage(this);
  Server copyWith(void Function(Server) updates) => super.copyWith((message) => updates(message as Server));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Server create() => Server._();
  Server createEmptyInstance() => create();
  static $pb.PbList<Server> createRepeated() => $pb.PbList<Server>();
  @$core.pragma('dart2js:noInline')
  static Server getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Server>(create);
  static Server _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get id => $_getIZ(0);
  @$pb.TagNumber(1)
  set id($core.int v) { $_setUnsignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);

  @$pb.TagNumber(2)
  $core.bool get running => $_getBF(1);
  @$pb.TagNumber(2)
  set running($core.bool v) { $_setBool(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasRunning() => $_has(1);
  @$pb.TagNumber(2)
  void clearRunning() => clearField(2);

  @$pb.TagNumber(3)
  Uptime get uptime => $_getN(2);
  @$pb.TagNumber(3)
  set uptime(Uptime v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasUptime() => $_has(2);
  @$pb.TagNumber(3)
  void clearUptime() => clearField(3);
  @$pb.TagNumber(3)
  Uptime ensureUptime() => $_ensure(2);
}

class Event extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Event', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..aOM<Server>(1, 'server', subBuilder: Server.create)
    ..e<Event_Type>(2, 'type', $pb.PbFieldType.OE, defaultOrMaker: Event_Type.ServerStopped, valueOf: Event_Type.valueOf, enumValues: Event_Type.values)
  ;

  Event._() : super();
  factory Event() => create();
  factory Event.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Event.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Event clone() => Event()..mergeFromMessage(this);
  Event copyWith(void Function(Event) updates) => super.copyWith((message) => updates(message as Event));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Event create() => Event._();
  Event createEmptyInstance() => create();
  static $pb.PbList<Event> createRepeated() => $pb.PbList<Event>();
  @$core.pragma('dart2js:noInline')
  static Event getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Event>(create);
  static Event _defaultInstance;

  @$pb.TagNumber(1)
  Server get server => $_getN(0);
  @$pb.TagNumber(1)
  set server(Server v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasServer() => $_has(0);
  @$pb.TagNumber(1)
  void clearServer() => clearField(1);
  @$pb.TagNumber(1)
  Server ensureServer() => $_ensure(0);

  @$pb.TagNumber(2)
  Event_Type get type => $_getN(1);
  @$pb.TagNumber(2)
  set type(Event_Type v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasType() => $_has(1);
  @$pb.TagNumber(2)
  void clearType() => clearField(2);
}

class ContextAction extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('ContextAction', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..aOM<Server>(1, 'server', subBuilder: Server.create)
    ..a<$core.int>(2, 'context', $pb.PbFieldType.OU3)
    ..aOS(3, 'action')
    ..aOS(4, 'text')
    ..aOM<User>(5, 'actor', subBuilder: User.create)
    ..aOM<User>(6, 'user', subBuilder: User.create)
    ..aOM<Channel>(7, 'channel', subBuilder: Channel.create)
  ;

  ContextAction._() : super();
  factory ContextAction() => create();
  factory ContextAction.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ContextAction.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  ContextAction clone() => ContextAction()..mergeFromMessage(this);
  ContextAction copyWith(void Function(ContextAction) updates) => super.copyWith((message) => updates(message as ContextAction));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ContextAction create() => ContextAction._();
  ContextAction createEmptyInstance() => create();
  static $pb.PbList<ContextAction> createRepeated() => $pb.PbList<ContextAction>();
  @$core.pragma('dart2js:noInline')
  static ContextAction getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ContextAction>(create);
  static ContextAction _defaultInstance;

  @$pb.TagNumber(1)
  Server get server => $_getN(0);
  @$pb.TagNumber(1)
  set server(Server v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasServer() => $_has(0);
  @$pb.TagNumber(1)
  void clearServer() => clearField(1);
  @$pb.TagNumber(1)
  Server ensureServer() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.int get context => $_getIZ(1);
  @$pb.TagNumber(2)
  set context($core.int v) { $_setUnsignedInt32(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasContext() => $_has(1);
  @$pb.TagNumber(2)
  void clearContext() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get action => $_getSZ(2);
  @$pb.TagNumber(3)
  set action($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasAction() => $_has(2);
  @$pb.TagNumber(3)
  void clearAction() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get text => $_getSZ(3);
  @$pb.TagNumber(4)
  set text($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasText() => $_has(3);
  @$pb.TagNumber(4)
  void clearText() => clearField(4);

  @$pb.TagNumber(5)
  User get actor => $_getN(4);
  @$pb.TagNumber(5)
  set actor(User v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasActor() => $_has(4);
  @$pb.TagNumber(5)
  void clearActor() => clearField(5);
  @$pb.TagNumber(5)
  User ensureActor() => $_ensure(4);

  @$pb.TagNumber(6)
  User get user => $_getN(5);
  @$pb.TagNumber(6)
  set user(User v) { setField(6, v); }
  @$pb.TagNumber(6)
  $core.bool hasUser() => $_has(5);
  @$pb.TagNumber(6)
  void clearUser() => clearField(6);
  @$pb.TagNumber(6)
  User ensureUser() => $_ensure(5);

  @$pb.TagNumber(7)
  Channel get channel => $_getN(6);
  @$pb.TagNumber(7)
  set channel(Channel v) { setField(7, v); }
  @$pb.TagNumber(7)
  $core.bool hasChannel() => $_has(6);
  @$pb.TagNumber(7)
  void clearChannel() => clearField(7);
  @$pb.TagNumber(7)
  Channel ensureChannel() => $_ensure(6);
}

class TextMessage_Filter extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('TextMessage.Filter', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..aOM<Server>(1, 'server', subBuilder: Server.create)
    ..e<TextMessage_Filter_Action>(2, 'action', $pb.PbFieldType.OE, defaultOrMaker: TextMessage_Filter_Action.Accept, valueOf: TextMessage_Filter_Action.valueOf, enumValues: TextMessage_Filter_Action.values)
    ..aOM<TextMessage>(3, 'message', subBuilder: TextMessage.create)
  ;

  TextMessage_Filter._() : super();
  factory TextMessage_Filter() => create();
  factory TextMessage_Filter.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory TextMessage_Filter.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  TextMessage_Filter clone() => TextMessage_Filter()..mergeFromMessage(this);
  TextMessage_Filter copyWith(void Function(TextMessage_Filter) updates) => super.copyWith((message) => updates(message as TextMessage_Filter));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static TextMessage_Filter create() => TextMessage_Filter._();
  TextMessage_Filter createEmptyInstance() => create();
  static $pb.PbList<TextMessage_Filter> createRepeated() => $pb.PbList<TextMessage_Filter>();
  @$core.pragma('dart2js:noInline')
  static TextMessage_Filter getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<TextMessage_Filter>(create);
  static TextMessage_Filter _defaultInstance;

  @$pb.TagNumber(1)
  Server get server => $_getN(0);
  @$pb.TagNumber(1)
  set server(Server v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasServer() => $_has(0);
  @$pb.TagNumber(1)
  void clearServer() => clearField(1);
  @$pb.TagNumber(1)
  Server ensureServer() => $_ensure(0);

  @$pb.TagNumber(2)
  TextMessage_Filter_Action get action => $_getN(1);
  @$pb.TagNumber(2)
  set action(TextMessage_Filter_Action v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasAction() => $_has(1);
  @$pb.TagNumber(2)
  void clearAction() => clearField(2);

  @$pb.TagNumber(3)
  TextMessage get message => $_getN(2);
  @$pb.TagNumber(3)
  set message(TextMessage v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasMessage() => $_has(2);
  @$pb.TagNumber(3)
  void clearMessage() => clearField(3);
  @$pb.TagNumber(3)
  TextMessage ensureMessage() => $_ensure(2);
}

class TextMessage extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('TextMessage', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..aOM<Server>(1, 'server', subBuilder: Server.create)
    ..aOM<User>(2, 'actor', subBuilder: User.create)
    ..pc<User>(3, 'users', $pb.PbFieldType.PM, subBuilder: User.create)
    ..pc<Channel>(4, 'channels', $pb.PbFieldType.PM, subBuilder: Channel.create)
    ..pc<Channel>(5, 'trees', $pb.PbFieldType.PM, subBuilder: Channel.create)
    ..aOS(6, 'text')
  ;

  TextMessage._() : super();
  factory TextMessage() => create();
  factory TextMessage.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory TextMessage.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  TextMessage clone() => TextMessage()..mergeFromMessage(this);
  TextMessage copyWith(void Function(TextMessage) updates) => super.copyWith((message) => updates(message as TextMessage));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static TextMessage create() => TextMessage._();
  TextMessage createEmptyInstance() => create();
  static $pb.PbList<TextMessage> createRepeated() => $pb.PbList<TextMessage>();
  @$core.pragma('dart2js:noInline')
  static TextMessage getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<TextMessage>(create);
  static TextMessage _defaultInstance;

  @$pb.TagNumber(1)
  Server get server => $_getN(0);
  @$pb.TagNumber(1)
  set server(Server v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasServer() => $_has(0);
  @$pb.TagNumber(1)
  void clearServer() => clearField(1);
  @$pb.TagNumber(1)
  Server ensureServer() => $_ensure(0);

  @$pb.TagNumber(2)
  User get actor => $_getN(1);
  @$pb.TagNumber(2)
  set actor(User v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasActor() => $_has(1);
  @$pb.TagNumber(2)
  void clearActor() => clearField(2);
  @$pb.TagNumber(2)
  User ensureActor() => $_ensure(1);

  @$pb.TagNumber(3)
  $core.List<User> get users => $_getList(2);

  @$pb.TagNumber(4)
  $core.List<Channel> get channels => $_getList(3);

  @$pb.TagNumber(5)
  $core.List<Channel> get trees => $_getList(4);

  @$pb.TagNumber(6)
  $core.String get text => $_getSZ(5);
  @$pb.TagNumber(6)
  set text($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasText() => $_has(5);
  @$pb.TagNumber(6)
  void clearText() => clearField(6);
}

class Log_Query extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Log.Query', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..aOM<Server>(1, 'server', subBuilder: Server.create)
    ..a<$core.int>(2, 'min', $pb.PbFieldType.OU3)
    ..a<$core.int>(3, 'max', $pb.PbFieldType.OU3)
  ;

  Log_Query._() : super();
  factory Log_Query() => create();
  factory Log_Query.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Log_Query.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Log_Query clone() => Log_Query()..mergeFromMessage(this);
  Log_Query copyWith(void Function(Log_Query) updates) => super.copyWith((message) => updates(message as Log_Query));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Log_Query create() => Log_Query._();
  Log_Query createEmptyInstance() => create();
  static $pb.PbList<Log_Query> createRepeated() => $pb.PbList<Log_Query>();
  @$core.pragma('dart2js:noInline')
  static Log_Query getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Log_Query>(create);
  static Log_Query _defaultInstance;

  @$pb.TagNumber(1)
  Server get server => $_getN(0);
  @$pb.TagNumber(1)
  set server(Server v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasServer() => $_has(0);
  @$pb.TagNumber(1)
  void clearServer() => clearField(1);
  @$pb.TagNumber(1)
  Server ensureServer() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.int get min => $_getIZ(1);
  @$pb.TagNumber(2)
  set min($core.int v) { $_setUnsignedInt32(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasMin() => $_has(1);
  @$pb.TagNumber(2)
  void clearMin() => clearField(2);

  @$pb.TagNumber(3)
  $core.int get max => $_getIZ(2);
  @$pb.TagNumber(3)
  set max($core.int v) { $_setUnsignedInt32(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasMax() => $_has(2);
  @$pb.TagNumber(3)
  void clearMax() => clearField(3);
}

class Log_List extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Log.List', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..aOM<Server>(1, 'server', subBuilder: Server.create)
    ..a<$core.int>(2, 'total', $pb.PbFieldType.OU3)
    ..a<$core.int>(3, 'min', $pb.PbFieldType.OU3)
    ..a<$core.int>(4, 'max', $pb.PbFieldType.OU3)
    ..pc<Log>(5, 'entries', $pb.PbFieldType.PM, subBuilder: Log.create)
  ;

  Log_List._() : super();
  factory Log_List() => create();
  factory Log_List.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Log_List.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Log_List clone() => Log_List()..mergeFromMessage(this);
  Log_List copyWith(void Function(Log_List) updates) => super.copyWith((message) => updates(message as Log_List));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Log_List create() => Log_List._();
  Log_List createEmptyInstance() => create();
  static $pb.PbList<Log_List> createRepeated() => $pb.PbList<Log_List>();
  @$core.pragma('dart2js:noInline')
  static Log_List getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Log_List>(create);
  static Log_List _defaultInstance;

  @$pb.TagNumber(1)
  Server get server => $_getN(0);
  @$pb.TagNumber(1)
  set server(Server v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasServer() => $_has(0);
  @$pb.TagNumber(1)
  void clearServer() => clearField(1);
  @$pb.TagNumber(1)
  Server ensureServer() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.int get total => $_getIZ(1);
  @$pb.TagNumber(2)
  set total($core.int v) { $_setUnsignedInt32(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasTotal() => $_has(1);
  @$pb.TagNumber(2)
  void clearTotal() => clearField(2);

  @$pb.TagNumber(3)
  $core.int get min => $_getIZ(2);
  @$pb.TagNumber(3)
  set min($core.int v) { $_setUnsignedInt32(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasMin() => $_has(2);
  @$pb.TagNumber(3)
  void clearMin() => clearField(3);

  @$pb.TagNumber(4)
  $core.int get max => $_getIZ(3);
  @$pb.TagNumber(4)
  set max($core.int v) { $_setUnsignedInt32(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasMax() => $_has(3);
  @$pb.TagNumber(4)
  void clearMax() => clearField(4);

  @$pb.TagNumber(5)
  $core.List<Log> get entries => $_getList(4);
}

class Log extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Log', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..aOM<Server>(1, 'server', subBuilder: Server.create)
    ..aInt64(2, 'timestamp')
    ..aOS(3, 'text')
  ;

  Log._() : super();
  factory Log() => create();
  factory Log.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Log.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Log clone() => Log()..mergeFromMessage(this);
  Log copyWith(void Function(Log) updates) => super.copyWith((message) => updates(message as Log));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Log create() => Log._();
  Log createEmptyInstance() => create();
  static $pb.PbList<Log> createRepeated() => $pb.PbList<Log>();
  @$core.pragma('dart2js:noInline')
  static Log getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Log>(create);
  static Log _defaultInstance;

  @$pb.TagNumber(1)
  Server get server => $_getN(0);
  @$pb.TagNumber(1)
  set server(Server v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasServer() => $_has(0);
  @$pb.TagNumber(1)
  void clearServer() => clearField(1);
  @$pb.TagNumber(1)
  Server ensureServer() => $_ensure(0);

  @$pb.TagNumber(2)
  $fixnum.Int64 get timestamp => $_getI64(1);
  @$pb.TagNumber(2)
  set timestamp($fixnum.Int64 v) { $_setInt64(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasTimestamp() => $_has(1);
  @$pb.TagNumber(2)
  void clearTimestamp() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get text => $_getSZ(2);
  @$pb.TagNumber(3)
  set text($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasText() => $_has(2);
  @$pb.TagNumber(3)
  void clearText() => clearField(3);
}

class Config_Field extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Config.Field', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..aOM<Server>(1, 'server', subBuilder: Server.create)
    ..aOS(2, 'key')
    ..aOS(3, 'value')
  ;

  Config_Field._() : super();
  factory Config_Field() => create();
  factory Config_Field.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Config_Field.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Config_Field clone() => Config_Field()..mergeFromMessage(this);
  Config_Field copyWith(void Function(Config_Field) updates) => super.copyWith((message) => updates(message as Config_Field));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Config_Field create() => Config_Field._();
  Config_Field createEmptyInstance() => create();
  static $pb.PbList<Config_Field> createRepeated() => $pb.PbList<Config_Field>();
  @$core.pragma('dart2js:noInline')
  static Config_Field getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Config_Field>(create);
  static Config_Field _defaultInstance;

  @$pb.TagNumber(1)
  Server get server => $_getN(0);
  @$pb.TagNumber(1)
  set server(Server v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasServer() => $_has(0);
  @$pb.TagNumber(1)
  void clearServer() => clearField(1);
  @$pb.TagNumber(1)
  Server ensureServer() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.String get key => $_getSZ(1);
  @$pb.TagNumber(2)
  set key($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasKey() => $_has(1);
  @$pb.TagNumber(2)
  void clearKey() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get value => $_getSZ(2);
  @$pb.TagNumber(3)
  set value($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasValue() => $_has(2);
  @$pb.TagNumber(3)
  void clearValue() => clearField(3);
}

class Config extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Config', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..aOM<Server>(1, 'server', subBuilder: Server.create)
    ..m<$core.String, $core.String>(2, 'fields', entryClassName: 'Config.FieldsEntry', keyFieldType: $pb.PbFieldType.OS, valueFieldType: $pb.PbFieldType.OS, packageName: const $pb.PackageName('MurmurRPC'))
  ;

  Config._() : super();
  factory Config() => create();
  factory Config.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Config.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Config clone() => Config()..mergeFromMessage(this);
  Config copyWith(void Function(Config) updates) => super.copyWith((message) => updates(message as Config));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Config create() => Config._();
  Config createEmptyInstance() => create();
  static $pb.PbList<Config> createRepeated() => $pb.PbList<Config>();
  @$core.pragma('dart2js:noInline')
  static Config getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Config>(create);
  static Config _defaultInstance;

  @$pb.TagNumber(1)
  Server get server => $_getN(0);
  @$pb.TagNumber(1)
  set server(Server v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasServer() => $_has(0);
  @$pb.TagNumber(1)
  void clearServer() => clearField(1);
  @$pb.TagNumber(1)
  Server ensureServer() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.Map<$core.String, $core.String> get fields => $_getMap(1);
}

class Channel_Query extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Channel.Query', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..aOM<Server>(1, 'server', subBuilder: Server.create)
  ;

  Channel_Query._() : super();
  factory Channel_Query() => create();
  factory Channel_Query.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Channel_Query.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Channel_Query clone() => Channel_Query()..mergeFromMessage(this);
  Channel_Query copyWith(void Function(Channel_Query) updates) => super.copyWith((message) => updates(message as Channel_Query));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Channel_Query create() => Channel_Query._();
  Channel_Query createEmptyInstance() => create();
  static $pb.PbList<Channel_Query> createRepeated() => $pb.PbList<Channel_Query>();
  @$core.pragma('dart2js:noInline')
  static Channel_Query getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Channel_Query>(create);
  static Channel_Query _defaultInstance;

  @$pb.TagNumber(1)
  Server get server => $_getN(0);
  @$pb.TagNumber(1)
  set server(Server v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasServer() => $_has(0);
  @$pb.TagNumber(1)
  void clearServer() => clearField(1);
  @$pb.TagNumber(1)
  Server ensureServer() => $_ensure(0);
}

class Channel_List extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Channel.List', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..aOM<Server>(1, 'server', subBuilder: Server.create)
    ..pc<Channel>(2, 'channels', $pb.PbFieldType.PM, subBuilder: Channel.create)
  ;

  Channel_List._() : super();
  factory Channel_List() => create();
  factory Channel_List.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Channel_List.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Channel_List clone() => Channel_List()..mergeFromMessage(this);
  Channel_List copyWith(void Function(Channel_List) updates) => super.copyWith((message) => updates(message as Channel_List));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Channel_List create() => Channel_List._();
  Channel_List createEmptyInstance() => create();
  static $pb.PbList<Channel_List> createRepeated() => $pb.PbList<Channel_List>();
  @$core.pragma('dart2js:noInline')
  static Channel_List getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Channel_List>(create);
  static Channel_List _defaultInstance;

  @$pb.TagNumber(1)
  Server get server => $_getN(0);
  @$pb.TagNumber(1)
  set server(Server v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasServer() => $_has(0);
  @$pb.TagNumber(1)
  void clearServer() => clearField(1);
  @$pb.TagNumber(1)
  Server ensureServer() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.List<Channel> get channels => $_getList(1);
}

class Channel extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Channel', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..aOM<Server>(1, 'server', subBuilder: Server.create)
    ..a<$core.int>(2, 'id', $pb.PbFieldType.OU3)
    ..aOS(3, 'name')
    ..aOM<Channel>(4, 'parent', subBuilder: Channel.create)
    ..pc<Channel>(5, 'links', $pb.PbFieldType.PM, subBuilder: Channel.create)
    ..aOS(6, 'description')
    ..aOB(7, 'temporary')
    ..a<$core.int>(8, 'position', $pb.PbFieldType.O3)
  ;

  Channel._() : super();
  factory Channel() => create();
  factory Channel.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Channel.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Channel clone() => Channel()..mergeFromMessage(this);
  Channel copyWith(void Function(Channel) updates) => super.copyWith((message) => updates(message as Channel));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Channel create() => Channel._();
  Channel createEmptyInstance() => create();
  static $pb.PbList<Channel> createRepeated() => $pb.PbList<Channel>();
  @$core.pragma('dart2js:noInline')
  static Channel getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Channel>(create);
  static Channel _defaultInstance;

  @$pb.TagNumber(1)
  Server get server => $_getN(0);
  @$pb.TagNumber(1)
  set server(Server v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasServer() => $_has(0);
  @$pb.TagNumber(1)
  void clearServer() => clearField(1);
  @$pb.TagNumber(1)
  Server ensureServer() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.int get id => $_getIZ(1);
  @$pb.TagNumber(2)
  set id($core.int v) { $_setUnsignedInt32(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasId() => $_has(1);
  @$pb.TagNumber(2)
  void clearId() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get name => $_getSZ(2);
  @$pb.TagNumber(3)
  set name($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasName() => $_has(2);
  @$pb.TagNumber(3)
  void clearName() => clearField(3);

  @$pb.TagNumber(4)
  Channel get parent => $_getN(3);
  @$pb.TagNumber(4)
  set parent(Channel v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasParent() => $_has(3);
  @$pb.TagNumber(4)
  void clearParent() => clearField(4);
  @$pb.TagNumber(4)
  Channel ensureParent() => $_ensure(3);

  @$pb.TagNumber(5)
  $core.List<Channel> get links => $_getList(4);

  @$pb.TagNumber(6)
  $core.String get description => $_getSZ(5);
  @$pb.TagNumber(6)
  set description($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasDescription() => $_has(5);
  @$pb.TagNumber(6)
  void clearDescription() => clearField(6);

  @$pb.TagNumber(7)
  $core.bool get temporary => $_getBF(6);
  @$pb.TagNumber(7)
  set temporary($core.bool v) { $_setBool(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasTemporary() => $_has(6);
  @$pb.TagNumber(7)
  void clearTemporary() => clearField(7);

  @$pb.TagNumber(8)
  $core.int get position => $_getIZ(7);
  @$pb.TagNumber(8)
  set position($core.int v) { $_setSignedInt32(7, v); }
  @$pb.TagNumber(8)
  $core.bool hasPosition() => $_has(7);
  @$pb.TagNumber(8)
  void clearPosition() => clearField(8);
}

class User_Query extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('User.Query', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..aOM<Server>(1, 'server', subBuilder: Server.create)
  ;

  User_Query._() : super();
  factory User_Query() => create();
  factory User_Query.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory User_Query.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  User_Query clone() => User_Query()..mergeFromMessage(this);
  User_Query copyWith(void Function(User_Query) updates) => super.copyWith((message) => updates(message as User_Query));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static User_Query create() => User_Query._();
  User_Query createEmptyInstance() => create();
  static $pb.PbList<User_Query> createRepeated() => $pb.PbList<User_Query>();
  @$core.pragma('dart2js:noInline')
  static User_Query getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<User_Query>(create);
  static User_Query _defaultInstance;

  @$pb.TagNumber(1)
  Server get server => $_getN(0);
  @$pb.TagNumber(1)
  set server(Server v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasServer() => $_has(0);
  @$pb.TagNumber(1)
  void clearServer() => clearField(1);
  @$pb.TagNumber(1)
  Server ensureServer() => $_ensure(0);
}

class User_List extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('User.List', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..aOM<Server>(1, 'server', subBuilder: Server.create)
    ..pc<User>(2, 'users', $pb.PbFieldType.PM, subBuilder: User.create)
  ;

  User_List._() : super();
  factory User_List() => create();
  factory User_List.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory User_List.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  User_List clone() => User_List()..mergeFromMessage(this);
  User_List copyWith(void Function(User_List) updates) => super.copyWith((message) => updates(message as User_List));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static User_List create() => User_List._();
  User_List createEmptyInstance() => create();
  static $pb.PbList<User_List> createRepeated() => $pb.PbList<User_List>();
  @$core.pragma('dart2js:noInline')
  static User_List getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<User_List>(create);
  static User_List _defaultInstance;

  @$pb.TagNumber(1)
  Server get server => $_getN(0);
  @$pb.TagNumber(1)
  set server(Server v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasServer() => $_has(0);
  @$pb.TagNumber(1)
  void clearServer() => clearField(1);
  @$pb.TagNumber(1)
  Server ensureServer() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.List<User> get users => $_getList(1);
}

class User_Kick extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('User.Kick', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..aOM<Server>(1, 'server', subBuilder: Server.create)
    ..aOM<User>(2, 'user', subBuilder: User.create)
    ..aOM<User>(3, 'actor', subBuilder: User.create)
    ..aOS(4, 'reason')
  ;

  User_Kick._() : super();
  factory User_Kick() => create();
  factory User_Kick.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory User_Kick.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  User_Kick clone() => User_Kick()..mergeFromMessage(this);
  User_Kick copyWith(void Function(User_Kick) updates) => super.copyWith((message) => updates(message as User_Kick));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static User_Kick create() => User_Kick._();
  User_Kick createEmptyInstance() => create();
  static $pb.PbList<User_Kick> createRepeated() => $pb.PbList<User_Kick>();
  @$core.pragma('dart2js:noInline')
  static User_Kick getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<User_Kick>(create);
  static User_Kick _defaultInstance;

  @$pb.TagNumber(1)
  Server get server => $_getN(0);
  @$pb.TagNumber(1)
  set server(Server v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasServer() => $_has(0);
  @$pb.TagNumber(1)
  void clearServer() => clearField(1);
  @$pb.TagNumber(1)
  Server ensureServer() => $_ensure(0);

  @$pb.TagNumber(2)
  User get user => $_getN(1);
  @$pb.TagNumber(2)
  set user(User v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasUser() => $_has(1);
  @$pb.TagNumber(2)
  void clearUser() => clearField(2);
  @$pb.TagNumber(2)
  User ensureUser() => $_ensure(1);

  @$pb.TagNumber(3)
  User get actor => $_getN(2);
  @$pb.TagNumber(3)
  set actor(User v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasActor() => $_has(2);
  @$pb.TagNumber(3)
  void clearActor() => clearField(3);
  @$pb.TagNumber(3)
  User ensureActor() => $_ensure(2);

  @$pb.TagNumber(4)
  $core.String get reason => $_getSZ(3);
  @$pb.TagNumber(4)
  set reason($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasReason() => $_has(3);
  @$pb.TagNumber(4)
  void clearReason() => clearField(4);
}

class User extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('User', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..aOM<Server>(1, 'server', subBuilder: Server.create)
    ..a<$core.int>(2, 'session', $pb.PbFieldType.OU3)
    ..a<$core.int>(3, 'id', $pb.PbFieldType.OU3)
    ..aOS(4, 'name')
    ..aOB(5, 'mute')
    ..aOB(6, 'deaf')
    ..aOB(7, 'suppress')
    ..aOB(8, 'prioritySpeaker')
    ..aOB(9, 'selfMute')
    ..aOB(10, 'selfDeaf')
    ..aOB(11, 'recording')
    ..aOM<Channel>(12, 'channel', subBuilder: Channel.create)
    ..a<$core.int>(13, 'onlineSecs', $pb.PbFieldType.OU3)
    ..a<$core.int>(14, 'idleSecs', $pb.PbFieldType.OU3)
    ..a<$core.int>(15, 'bytesPerSec', $pb.PbFieldType.OU3)
    ..aOM<Version>(16, 'version', subBuilder: Version.create)
    ..a<$core.List<$core.int>>(17, 'pluginContext', $pb.PbFieldType.OY)
    ..aOS(18, 'pluginIdentity')
    ..aOS(19, 'comment')
    ..a<$core.List<$core.int>>(20, 'texture', $pb.PbFieldType.OY)
    ..a<$core.List<$core.int>>(21, 'address', $pb.PbFieldType.OY)
    ..aOB(22, 'tcpOnly')
    ..a<$core.double>(23, 'udpPingMsecs', $pb.PbFieldType.OF)
    ..a<$core.double>(24, 'tcpPingMsecs', $pb.PbFieldType.OF)
  ;

  User._() : super();
  factory User() => create();
  factory User.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory User.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  User clone() => User()..mergeFromMessage(this);
  User copyWith(void Function(User) updates) => super.copyWith((message) => updates(message as User));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static User create() => User._();
  User createEmptyInstance() => create();
  static $pb.PbList<User> createRepeated() => $pb.PbList<User>();
  @$core.pragma('dart2js:noInline')
  static User getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<User>(create);
  static User _defaultInstance;

  @$pb.TagNumber(1)
  Server get server => $_getN(0);
  @$pb.TagNumber(1)
  set server(Server v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasServer() => $_has(0);
  @$pb.TagNumber(1)
  void clearServer() => clearField(1);
  @$pb.TagNumber(1)
  Server ensureServer() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.int get session => $_getIZ(1);
  @$pb.TagNumber(2)
  set session($core.int v) { $_setUnsignedInt32(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasSession() => $_has(1);
  @$pb.TagNumber(2)
  void clearSession() => clearField(2);

  @$pb.TagNumber(3)
  $core.int get id => $_getIZ(2);
  @$pb.TagNumber(3)
  set id($core.int v) { $_setUnsignedInt32(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasId() => $_has(2);
  @$pb.TagNumber(3)
  void clearId() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get name => $_getSZ(3);
  @$pb.TagNumber(4)
  set name($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasName() => $_has(3);
  @$pb.TagNumber(4)
  void clearName() => clearField(4);

  @$pb.TagNumber(5)
  $core.bool get mute => $_getBF(4);
  @$pb.TagNumber(5)
  set mute($core.bool v) { $_setBool(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasMute() => $_has(4);
  @$pb.TagNumber(5)
  void clearMute() => clearField(5);

  @$pb.TagNumber(6)
  $core.bool get deaf => $_getBF(5);
  @$pb.TagNumber(6)
  set deaf($core.bool v) { $_setBool(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasDeaf() => $_has(5);
  @$pb.TagNumber(6)
  void clearDeaf() => clearField(6);

  @$pb.TagNumber(7)
  $core.bool get suppress => $_getBF(6);
  @$pb.TagNumber(7)
  set suppress($core.bool v) { $_setBool(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasSuppress() => $_has(6);
  @$pb.TagNumber(7)
  void clearSuppress() => clearField(7);

  @$pb.TagNumber(8)
  $core.bool get prioritySpeaker => $_getBF(7);
  @$pb.TagNumber(8)
  set prioritySpeaker($core.bool v) { $_setBool(7, v); }
  @$pb.TagNumber(8)
  $core.bool hasPrioritySpeaker() => $_has(7);
  @$pb.TagNumber(8)
  void clearPrioritySpeaker() => clearField(8);

  @$pb.TagNumber(9)
  $core.bool get selfMute => $_getBF(8);
  @$pb.TagNumber(9)
  set selfMute($core.bool v) { $_setBool(8, v); }
  @$pb.TagNumber(9)
  $core.bool hasSelfMute() => $_has(8);
  @$pb.TagNumber(9)
  void clearSelfMute() => clearField(9);

  @$pb.TagNumber(10)
  $core.bool get selfDeaf => $_getBF(9);
  @$pb.TagNumber(10)
  set selfDeaf($core.bool v) { $_setBool(9, v); }
  @$pb.TagNumber(10)
  $core.bool hasSelfDeaf() => $_has(9);
  @$pb.TagNumber(10)
  void clearSelfDeaf() => clearField(10);

  @$pb.TagNumber(11)
  $core.bool get recording => $_getBF(10);
  @$pb.TagNumber(11)
  set recording($core.bool v) { $_setBool(10, v); }
  @$pb.TagNumber(11)
  $core.bool hasRecording() => $_has(10);
  @$pb.TagNumber(11)
  void clearRecording() => clearField(11);

  @$pb.TagNumber(12)
  Channel get channel => $_getN(11);
  @$pb.TagNumber(12)
  set channel(Channel v) { setField(12, v); }
  @$pb.TagNumber(12)
  $core.bool hasChannel() => $_has(11);
  @$pb.TagNumber(12)
  void clearChannel() => clearField(12);
  @$pb.TagNumber(12)
  Channel ensureChannel() => $_ensure(11);

  @$pb.TagNumber(13)
  $core.int get onlineSecs => $_getIZ(12);
  @$pb.TagNumber(13)
  set onlineSecs($core.int v) { $_setUnsignedInt32(12, v); }
  @$pb.TagNumber(13)
  $core.bool hasOnlineSecs() => $_has(12);
  @$pb.TagNumber(13)
  void clearOnlineSecs() => clearField(13);

  @$pb.TagNumber(14)
  $core.int get idleSecs => $_getIZ(13);
  @$pb.TagNumber(14)
  set idleSecs($core.int v) { $_setUnsignedInt32(13, v); }
  @$pb.TagNumber(14)
  $core.bool hasIdleSecs() => $_has(13);
  @$pb.TagNumber(14)
  void clearIdleSecs() => clearField(14);

  @$pb.TagNumber(15)
  $core.int get bytesPerSec => $_getIZ(14);
  @$pb.TagNumber(15)
  set bytesPerSec($core.int v) { $_setUnsignedInt32(14, v); }
  @$pb.TagNumber(15)
  $core.bool hasBytesPerSec() => $_has(14);
  @$pb.TagNumber(15)
  void clearBytesPerSec() => clearField(15);

  @$pb.TagNumber(16)
  Version get version => $_getN(15);
  @$pb.TagNumber(16)
  set version(Version v) { setField(16, v); }
  @$pb.TagNumber(16)
  $core.bool hasVersion() => $_has(15);
  @$pb.TagNumber(16)
  void clearVersion() => clearField(16);
  @$pb.TagNumber(16)
  Version ensureVersion() => $_ensure(15);

  @$pb.TagNumber(17)
  $core.List<$core.int> get pluginContext => $_getN(16);
  @$pb.TagNumber(17)
  set pluginContext($core.List<$core.int> v) { $_setBytes(16, v); }
  @$pb.TagNumber(17)
  $core.bool hasPluginContext() => $_has(16);
  @$pb.TagNumber(17)
  void clearPluginContext() => clearField(17);

  @$pb.TagNumber(18)
  $core.String get pluginIdentity => $_getSZ(17);
  @$pb.TagNumber(18)
  set pluginIdentity($core.String v) { $_setString(17, v); }
  @$pb.TagNumber(18)
  $core.bool hasPluginIdentity() => $_has(17);
  @$pb.TagNumber(18)
  void clearPluginIdentity() => clearField(18);

  @$pb.TagNumber(19)
  $core.String get comment => $_getSZ(18);
  @$pb.TagNumber(19)
  set comment($core.String v) { $_setString(18, v); }
  @$pb.TagNumber(19)
  $core.bool hasComment() => $_has(18);
  @$pb.TagNumber(19)
  void clearComment() => clearField(19);

  @$pb.TagNumber(20)
  $core.List<$core.int> get texture => $_getN(19);
  @$pb.TagNumber(20)
  set texture($core.List<$core.int> v) { $_setBytes(19, v); }
  @$pb.TagNumber(20)
  $core.bool hasTexture() => $_has(19);
  @$pb.TagNumber(20)
  void clearTexture() => clearField(20);

  @$pb.TagNumber(21)
  $core.List<$core.int> get address => $_getN(20);
  @$pb.TagNumber(21)
  set address($core.List<$core.int> v) { $_setBytes(20, v); }
  @$pb.TagNumber(21)
  $core.bool hasAddress() => $_has(20);
  @$pb.TagNumber(21)
  void clearAddress() => clearField(21);

  @$pb.TagNumber(22)
  $core.bool get tcpOnly => $_getBF(21);
  @$pb.TagNumber(22)
  set tcpOnly($core.bool v) { $_setBool(21, v); }
  @$pb.TagNumber(22)
  $core.bool hasTcpOnly() => $_has(21);
  @$pb.TagNumber(22)
  void clearTcpOnly() => clearField(22);

  @$pb.TagNumber(23)
  $core.double get udpPingMsecs => $_getN(22);
  @$pb.TagNumber(23)
  set udpPingMsecs($core.double v) { $_setFloat(22, v); }
  @$pb.TagNumber(23)
  $core.bool hasUdpPingMsecs() => $_has(22);
  @$pb.TagNumber(23)
  void clearUdpPingMsecs() => clearField(23);

  @$pb.TagNumber(24)
  $core.double get tcpPingMsecs => $_getN(23);
  @$pb.TagNumber(24)
  set tcpPingMsecs($core.double v) { $_setFloat(23, v); }
  @$pb.TagNumber(24)
  $core.bool hasTcpPingMsecs() => $_has(23);
  @$pb.TagNumber(24)
  void clearTcpPingMsecs() => clearField(24);
}

class Tree_Query extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Tree.Query', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..aOM<Server>(1, 'server', subBuilder: Server.create)
  ;

  Tree_Query._() : super();
  factory Tree_Query() => create();
  factory Tree_Query.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Tree_Query.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Tree_Query clone() => Tree_Query()..mergeFromMessage(this);
  Tree_Query copyWith(void Function(Tree_Query) updates) => super.copyWith((message) => updates(message as Tree_Query));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Tree_Query create() => Tree_Query._();
  Tree_Query createEmptyInstance() => create();
  static $pb.PbList<Tree_Query> createRepeated() => $pb.PbList<Tree_Query>();
  @$core.pragma('dart2js:noInline')
  static Tree_Query getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Tree_Query>(create);
  static Tree_Query _defaultInstance;

  @$pb.TagNumber(1)
  Server get server => $_getN(0);
  @$pb.TagNumber(1)
  set server(Server v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasServer() => $_has(0);
  @$pb.TagNumber(1)
  void clearServer() => clearField(1);
  @$pb.TagNumber(1)
  Server ensureServer() => $_ensure(0);
}

class Tree extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Tree', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..aOM<Server>(1, 'server', subBuilder: Server.create)
    ..aOM<Channel>(2, 'channel', subBuilder: Channel.create)
    ..pc<Tree>(3, 'children', $pb.PbFieldType.PM, subBuilder: Tree.create)
    ..pc<User>(4, 'users', $pb.PbFieldType.PM, subBuilder: User.create)
  ;

  Tree._() : super();
  factory Tree() => create();
  factory Tree.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Tree.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Tree clone() => Tree()..mergeFromMessage(this);
  Tree copyWith(void Function(Tree) updates) => super.copyWith((message) => updates(message as Tree));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Tree create() => Tree._();
  Tree createEmptyInstance() => create();
  static $pb.PbList<Tree> createRepeated() => $pb.PbList<Tree>();
  @$core.pragma('dart2js:noInline')
  static Tree getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Tree>(create);
  static Tree _defaultInstance;

  @$pb.TagNumber(1)
  Server get server => $_getN(0);
  @$pb.TagNumber(1)
  set server(Server v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasServer() => $_has(0);
  @$pb.TagNumber(1)
  void clearServer() => clearField(1);
  @$pb.TagNumber(1)
  Server ensureServer() => $_ensure(0);

  @$pb.TagNumber(2)
  Channel get channel => $_getN(1);
  @$pb.TagNumber(2)
  set channel(Channel v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasChannel() => $_has(1);
  @$pb.TagNumber(2)
  void clearChannel() => clearField(2);
  @$pb.TagNumber(2)
  Channel ensureChannel() => $_ensure(1);

  @$pb.TagNumber(3)
  $core.List<Tree> get children => $_getList(2);

  @$pb.TagNumber(4)
  $core.List<User> get users => $_getList(3);
}

class Ban_Query extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Ban.Query', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..aOM<Server>(1, 'server', subBuilder: Server.create)
  ;

  Ban_Query._() : super();
  factory Ban_Query() => create();
  factory Ban_Query.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Ban_Query.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Ban_Query clone() => Ban_Query()..mergeFromMessage(this);
  Ban_Query copyWith(void Function(Ban_Query) updates) => super.copyWith((message) => updates(message as Ban_Query));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Ban_Query create() => Ban_Query._();
  Ban_Query createEmptyInstance() => create();
  static $pb.PbList<Ban_Query> createRepeated() => $pb.PbList<Ban_Query>();
  @$core.pragma('dart2js:noInline')
  static Ban_Query getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Ban_Query>(create);
  static Ban_Query _defaultInstance;

  @$pb.TagNumber(1)
  Server get server => $_getN(0);
  @$pb.TagNumber(1)
  set server(Server v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasServer() => $_has(0);
  @$pb.TagNumber(1)
  void clearServer() => clearField(1);
  @$pb.TagNumber(1)
  Server ensureServer() => $_ensure(0);
}

class Ban_List extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Ban.List', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..aOM<Server>(1, 'server', subBuilder: Server.create)
    ..pc<Ban>(2, 'bans', $pb.PbFieldType.PM, subBuilder: Ban.create)
  ;

  Ban_List._() : super();
  factory Ban_List() => create();
  factory Ban_List.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Ban_List.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Ban_List clone() => Ban_List()..mergeFromMessage(this);
  Ban_List copyWith(void Function(Ban_List) updates) => super.copyWith((message) => updates(message as Ban_List));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Ban_List create() => Ban_List._();
  Ban_List createEmptyInstance() => create();
  static $pb.PbList<Ban_List> createRepeated() => $pb.PbList<Ban_List>();
  @$core.pragma('dart2js:noInline')
  static Ban_List getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Ban_List>(create);
  static Ban_List _defaultInstance;

  @$pb.TagNumber(1)
  Server get server => $_getN(0);
  @$pb.TagNumber(1)
  set server(Server v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasServer() => $_has(0);
  @$pb.TagNumber(1)
  void clearServer() => clearField(1);
  @$pb.TagNumber(1)
  Server ensureServer() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.List<Ban> get bans => $_getList(1);
}

class Ban extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Ban', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..aOM<Server>(1, 'server', subBuilder: Server.create)
    ..a<$core.List<$core.int>>(2, 'address', $pb.PbFieldType.OY)
    ..a<$core.int>(3, 'bits', $pb.PbFieldType.OU3)
    ..aOS(4, 'name')
    ..aOS(5, 'hash')
    ..aOS(6, 'reason')
    ..aInt64(7, 'start')
    ..aInt64(8, 'durationSecs')
  ;

  Ban._() : super();
  factory Ban() => create();
  factory Ban.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Ban.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Ban clone() => Ban()..mergeFromMessage(this);
  Ban copyWith(void Function(Ban) updates) => super.copyWith((message) => updates(message as Ban));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Ban create() => Ban._();
  Ban createEmptyInstance() => create();
  static $pb.PbList<Ban> createRepeated() => $pb.PbList<Ban>();
  @$core.pragma('dart2js:noInline')
  static Ban getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Ban>(create);
  static Ban _defaultInstance;

  @$pb.TagNumber(1)
  Server get server => $_getN(0);
  @$pb.TagNumber(1)
  set server(Server v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasServer() => $_has(0);
  @$pb.TagNumber(1)
  void clearServer() => clearField(1);
  @$pb.TagNumber(1)
  Server ensureServer() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.List<$core.int> get address => $_getN(1);
  @$pb.TagNumber(2)
  set address($core.List<$core.int> v) { $_setBytes(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasAddress() => $_has(1);
  @$pb.TagNumber(2)
  void clearAddress() => clearField(2);

  @$pb.TagNumber(3)
  $core.int get bits => $_getIZ(2);
  @$pb.TagNumber(3)
  set bits($core.int v) { $_setUnsignedInt32(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasBits() => $_has(2);
  @$pb.TagNumber(3)
  void clearBits() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get name => $_getSZ(3);
  @$pb.TagNumber(4)
  set name($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasName() => $_has(3);
  @$pb.TagNumber(4)
  void clearName() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get hash => $_getSZ(4);
  @$pb.TagNumber(5)
  set hash($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasHash() => $_has(4);
  @$pb.TagNumber(5)
  void clearHash() => clearField(5);

  @$pb.TagNumber(6)
  $core.String get reason => $_getSZ(5);
  @$pb.TagNumber(6)
  set reason($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasReason() => $_has(5);
  @$pb.TagNumber(6)
  void clearReason() => clearField(6);

  @$pb.TagNumber(7)
  $fixnum.Int64 get start => $_getI64(6);
  @$pb.TagNumber(7)
  set start($fixnum.Int64 v) { $_setInt64(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasStart() => $_has(6);
  @$pb.TagNumber(7)
  void clearStart() => clearField(7);

  @$pb.TagNumber(8)
  $fixnum.Int64 get durationSecs => $_getI64(7);
  @$pb.TagNumber(8)
  set durationSecs($fixnum.Int64 v) { $_setInt64(7, v); }
  @$pb.TagNumber(8)
  $core.bool hasDurationSecs() => $_has(7);
  @$pb.TagNumber(8)
  void clearDurationSecs() => clearField(8);
}

class ACL_Group extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('ACL.Group', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..aOS(1, 'name')
    ..aOB(2, 'inherited')
    ..aOB(3, 'inherit')
    ..aOB(4, 'inheritable')
    ..pc<DatabaseUser>(5, 'usersAdd', $pb.PbFieldType.PM, subBuilder: DatabaseUser.create)
    ..pc<DatabaseUser>(6, 'usersRemove', $pb.PbFieldType.PM, subBuilder: DatabaseUser.create)
    ..pc<DatabaseUser>(7, 'users', $pb.PbFieldType.PM, subBuilder: DatabaseUser.create)
  ;

  ACL_Group._() : super();
  factory ACL_Group() => create();
  factory ACL_Group.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ACL_Group.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  ACL_Group clone() => ACL_Group()..mergeFromMessage(this);
  ACL_Group copyWith(void Function(ACL_Group) updates) => super.copyWith((message) => updates(message as ACL_Group));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ACL_Group create() => ACL_Group._();
  ACL_Group createEmptyInstance() => create();
  static $pb.PbList<ACL_Group> createRepeated() => $pb.PbList<ACL_Group>();
  @$core.pragma('dart2js:noInline')
  static ACL_Group getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ACL_Group>(create);
  static ACL_Group _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get name => $_getSZ(0);
  @$pb.TagNumber(1)
  set name($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasName() => $_has(0);
  @$pb.TagNumber(1)
  void clearName() => clearField(1);

  @$pb.TagNumber(2)
  $core.bool get inherited => $_getBF(1);
  @$pb.TagNumber(2)
  set inherited($core.bool v) { $_setBool(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasInherited() => $_has(1);
  @$pb.TagNumber(2)
  void clearInherited() => clearField(2);

  @$pb.TagNumber(3)
  $core.bool get inherit => $_getBF(2);
  @$pb.TagNumber(3)
  set inherit($core.bool v) { $_setBool(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasInherit() => $_has(2);
  @$pb.TagNumber(3)
  void clearInherit() => clearField(3);

  @$pb.TagNumber(4)
  $core.bool get inheritable => $_getBF(3);
  @$pb.TagNumber(4)
  set inheritable($core.bool v) { $_setBool(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasInheritable() => $_has(3);
  @$pb.TagNumber(4)
  void clearInheritable() => clearField(4);

  @$pb.TagNumber(5)
  $core.List<DatabaseUser> get usersAdd => $_getList(4);

  @$pb.TagNumber(6)
  $core.List<DatabaseUser> get usersRemove => $_getList(5);

  @$pb.TagNumber(7)
  $core.List<DatabaseUser> get users => $_getList(6);
}

class ACL_Query extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('ACL.Query', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..aOM<Server>(1, 'server', subBuilder: Server.create)
    ..aOM<User>(2, 'user', subBuilder: User.create)
    ..aOM<Channel>(3, 'channel', subBuilder: Channel.create)
  ;

  ACL_Query._() : super();
  factory ACL_Query() => create();
  factory ACL_Query.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ACL_Query.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  ACL_Query clone() => ACL_Query()..mergeFromMessage(this);
  ACL_Query copyWith(void Function(ACL_Query) updates) => super.copyWith((message) => updates(message as ACL_Query));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ACL_Query create() => ACL_Query._();
  ACL_Query createEmptyInstance() => create();
  static $pb.PbList<ACL_Query> createRepeated() => $pb.PbList<ACL_Query>();
  @$core.pragma('dart2js:noInline')
  static ACL_Query getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ACL_Query>(create);
  static ACL_Query _defaultInstance;

  @$pb.TagNumber(1)
  Server get server => $_getN(0);
  @$pb.TagNumber(1)
  set server(Server v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasServer() => $_has(0);
  @$pb.TagNumber(1)
  void clearServer() => clearField(1);
  @$pb.TagNumber(1)
  Server ensureServer() => $_ensure(0);

  @$pb.TagNumber(2)
  User get user => $_getN(1);
  @$pb.TagNumber(2)
  set user(User v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasUser() => $_has(1);
  @$pb.TagNumber(2)
  void clearUser() => clearField(2);
  @$pb.TagNumber(2)
  User ensureUser() => $_ensure(1);

  @$pb.TagNumber(3)
  Channel get channel => $_getN(2);
  @$pb.TagNumber(3)
  set channel(Channel v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasChannel() => $_has(2);
  @$pb.TagNumber(3)
  void clearChannel() => clearField(3);
  @$pb.TagNumber(3)
  Channel ensureChannel() => $_ensure(2);
}

class ACL_List extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('ACL.List', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..aOM<Server>(1, 'server', subBuilder: Server.create)
    ..aOM<Channel>(2, 'channel', subBuilder: Channel.create)
    ..pc<ACL>(3, 'acls', $pb.PbFieldType.PM, subBuilder: ACL.create)
    ..pc<ACL_Group>(4, 'groups', $pb.PbFieldType.PM, subBuilder: ACL_Group.create)
    ..aOB(5, 'inherit')
  ;

  ACL_List._() : super();
  factory ACL_List() => create();
  factory ACL_List.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ACL_List.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  ACL_List clone() => ACL_List()..mergeFromMessage(this);
  ACL_List copyWith(void Function(ACL_List) updates) => super.copyWith((message) => updates(message as ACL_List));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ACL_List create() => ACL_List._();
  ACL_List createEmptyInstance() => create();
  static $pb.PbList<ACL_List> createRepeated() => $pb.PbList<ACL_List>();
  @$core.pragma('dart2js:noInline')
  static ACL_List getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ACL_List>(create);
  static ACL_List _defaultInstance;

  @$pb.TagNumber(1)
  Server get server => $_getN(0);
  @$pb.TagNumber(1)
  set server(Server v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasServer() => $_has(0);
  @$pb.TagNumber(1)
  void clearServer() => clearField(1);
  @$pb.TagNumber(1)
  Server ensureServer() => $_ensure(0);

  @$pb.TagNumber(2)
  Channel get channel => $_getN(1);
  @$pb.TagNumber(2)
  set channel(Channel v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasChannel() => $_has(1);
  @$pb.TagNumber(2)
  void clearChannel() => clearField(2);
  @$pb.TagNumber(2)
  Channel ensureChannel() => $_ensure(1);

  @$pb.TagNumber(3)
  $core.List<ACL> get acls => $_getList(2);

  @$pb.TagNumber(4)
  $core.List<ACL_Group> get groups => $_getList(3);

  @$pb.TagNumber(5)
  $core.bool get inherit => $_getBF(4);
  @$pb.TagNumber(5)
  set inherit($core.bool v) { $_setBool(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasInherit() => $_has(4);
  @$pb.TagNumber(5)
  void clearInherit() => clearField(5);
}

class ACL_TemporaryGroup extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('ACL.TemporaryGroup', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..aOM<Server>(1, 'server', subBuilder: Server.create)
    ..aOM<Channel>(2, 'channel', subBuilder: Channel.create)
    ..aOM<User>(3, 'user', subBuilder: User.create)
    ..aOS(4, 'name')
  ;

  ACL_TemporaryGroup._() : super();
  factory ACL_TemporaryGroup() => create();
  factory ACL_TemporaryGroup.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ACL_TemporaryGroup.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  ACL_TemporaryGroup clone() => ACL_TemporaryGroup()..mergeFromMessage(this);
  ACL_TemporaryGroup copyWith(void Function(ACL_TemporaryGroup) updates) => super.copyWith((message) => updates(message as ACL_TemporaryGroup));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ACL_TemporaryGroup create() => ACL_TemporaryGroup._();
  ACL_TemporaryGroup createEmptyInstance() => create();
  static $pb.PbList<ACL_TemporaryGroup> createRepeated() => $pb.PbList<ACL_TemporaryGroup>();
  @$core.pragma('dart2js:noInline')
  static ACL_TemporaryGroup getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ACL_TemporaryGroup>(create);
  static ACL_TemporaryGroup _defaultInstance;

  @$pb.TagNumber(1)
  Server get server => $_getN(0);
  @$pb.TagNumber(1)
  set server(Server v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasServer() => $_has(0);
  @$pb.TagNumber(1)
  void clearServer() => clearField(1);
  @$pb.TagNumber(1)
  Server ensureServer() => $_ensure(0);

  @$pb.TagNumber(2)
  Channel get channel => $_getN(1);
  @$pb.TagNumber(2)
  set channel(Channel v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasChannel() => $_has(1);
  @$pb.TagNumber(2)
  void clearChannel() => clearField(2);
  @$pb.TagNumber(2)
  Channel ensureChannel() => $_ensure(1);

  @$pb.TagNumber(3)
  User get user => $_getN(2);
  @$pb.TagNumber(3)
  set user(User v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasUser() => $_has(2);
  @$pb.TagNumber(3)
  void clearUser() => clearField(3);
  @$pb.TagNumber(3)
  User ensureUser() => $_ensure(2);

  @$pb.TagNumber(4)
  $core.String get name => $_getSZ(3);
  @$pb.TagNumber(4)
  set name($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasName() => $_has(3);
  @$pb.TagNumber(4)
  void clearName() => clearField(4);
}

class ACL extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('ACL', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..aOB(3, 'applyHere')
    ..aOB(4, 'applySubs')
    ..aOB(5, 'inherited')
    ..aOM<DatabaseUser>(6, 'user', subBuilder: DatabaseUser.create)
    ..aOM<ACL_Group>(7, 'group', subBuilder: ACL_Group.create)
    ..a<$core.int>(8, 'allow', $pb.PbFieldType.OU3)
    ..a<$core.int>(9, 'deny', $pb.PbFieldType.OU3)
  ;

  ACL._() : super();
  factory ACL() => create();
  factory ACL.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ACL.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  ACL clone() => ACL()..mergeFromMessage(this);
  ACL copyWith(void Function(ACL) updates) => super.copyWith((message) => updates(message as ACL));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ACL create() => ACL._();
  ACL createEmptyInstance() => create();
  static $pb.PbList<ACL> createRepeated() => $pb.PbList<ACL>();
  @$core.pragma('dart2js:noInline')
  static ACL getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ACL>(create);
  static ACL _defaultInstance;

  @$pb.TagNumber(3)
  $core.bool get applyHere => $_getBF(0);
  @$pb.TagNumber(3)
  set applyHere($core.bool v) { $_setBool(0, v); }
  @$pb.TagNumber(3)
  $core.bool hasApplyHere() => $_has(0);
  @$pb.TagNumber(3)
  void clearApplyHere() => clearField(3);

  @$pb.TagNumber(4)
  $core.bool get applySubs => $_getBF(1);
  @$pb.TagNumber(4)
  set applySubs($core.bool v) { $_setBool(1, v); }
  @$pb.TagNumber(4)
  $core.bool hasApplySubs() => $_has(1);
  @$pb.TagNumber(4)
  void clearApplySubs() => clearField(4);

  @$pb.TagNumber(5)
  $core.bool get inherited => $_getBF(2);
  @$pb.TagNumber(5)
  set inherited($core.bool v) { $_setBool(2, v); }
  @$pb.TagNumber(5)
  $core.bool hasInherited() => $_has(2);
  @$pb.TagNumber(5)
  void clearInherited() => clearField(5);

  @$pb.TagNumber(6)
  DatabaseUser get user => $_getN(3);
  @$pb.TagNumber(6)
  set user(DatabaseUser v) { setField(6, v); }
  @$pb.TagNumber(6)
  $core.bool hasUser() => $_has(3);
  @$pb.TagNumber(6)
  void clearUser() => clearField(6);
  @$pb.TagNumber(6)
  DatabaseUser ensureUser() => $_ensure(3);

  @$pb.TagNumber(7)
  ACL_Group get group => $_getN(4);
  @$pb.TagNumber(7)
  set group(ACL_Group v) { setField(7, v); }
  @$pb.TagNumber(7)
  $core.bool hasGroup() => $_has(4);
  @$pb.TagNumber(7)
  void clearGroup() => clearField(7);
  @$pb.TagNumber(7)
  ACL_Group ensureGroup() => $_ensure(4);

  @$pb.TagNumber(8)
  $core.int get allow => $_getIZ(5);
  @$pb.TagNumber(8)
  set allow($core.int v) { $_setUnsignedInt32(5, v); }
  @$pb.TagNumber(8)
  $core.bool hasAllow() => $_has(5);
  @$pb.TagNumber(8)
  void clearAllow() => clearField(8);

  @$pb.TagNumber(9)
  $core.int get deny => $_getIZ(6);
  @$pb.TagNumber(9)
  set deny($core.int v) { $_setUnsignedInt32(6, v); }
  @$pb.TagNumber(9)
  $core.bool hasDeny() => $_has(6);
  @$pb.TagNumber(9)
  void clearDeny() => clearField(9);
}

class Authenticator_Request_Authenticate extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Authenticator.Request.Authenticate', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..aOS(1, 'name')
    ..aOS(2, 'password')
    ..p<$core.List<$core.int>>(3, 'certificates', $pb.PbFieldType.PY)
    ..aOS(4, 'certificateHash')
    ..aOB(5, 'strongCertificate')
    ..hasRequiredFields = false
  ;

  Authenticator_Request_Authenticate._() : super();
  factory Authenticator_Request_Authenticate() => create();
  factory Authenticator_Request_Authenticate.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Authenticator_Request_Authenticate.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Authenticator_Request_Authenticate clone() => Authenticator_Request_Authenticate()..mergeFromMessage(this);
  Authenticator_Request_Authenticate copyWith(void Function(Authenticator_Request_Authenticate) updates) => super.copyWith((message) => updates(message as Authenticator_Request_Authenticate));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Authenticator_Request_Authenticate create() => Authenticator_Request_Authenticate._();
  Authenticator_Request_Authenticate createEmptyInstance() => create();
  static $pb.PbList<Authenticator_Request_Authenticate> createRepeated() => $pb.PbList<Authenticator_Request_Authenticate>();
  @$core.pragma('dart2js:noInline')
  static Authenticator_Request_Authenticate getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Authenticator_Request_Authenticate>(create);
  static Authenticator_Request_Authenticate _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get name => $_getSZ(0);
  @$pb.TagNumber(1)
  set name($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasName() => $_has(0);
  @$pb.TagNumber(1)
  void clearName() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get password => $_getSZ(1);
  @$pb.TagNumber(2)
  set password($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasPassword() => $_has(1);
  @$pb.TagNumber(2)
  void clearPassword() => clearField(2);

  @$pb.TagNumber(3)
  $core.List<$core.List<$core.int>> get certificates => $_getList(2);

  @$pb.TagNumber(4)
  $core.String get certificateHash => $_getSZ(3);
  @$pb.TagNumber(4)
  set certificateHash($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasCertificateHash() => $_has(3);
  @$pb.TagNumber(4)
  void clearCertificateHash() => clearField(4);

  @$pb.TagNumber(5)
  $core.bool get strongCertificate => $_getBF(4);
  @$pb.TagNumber(5)
  set strongCertificate($core.bool v) { $_setBool(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasStrongCertificate() => $_has(4);
  @$pb.TagNumber(5)
  void clearStrongCertificate() => clearField(5);
}

class Authenticator_Request_Find extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Authenticator.Request.Find', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..a<$core.int>(1, 'id', $pb.PbFieldType.OU3)
    ..aOS(2, 'name')
    ..hasRequiredFields = false
  ;

  Authenticator_Request_Find._() : super();
  factory Authenticator_Request_Find() => create();
  factory Authenticator_Request_Find.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Authenticator_Request_Find.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Authenticator_Request_Find clone() => Authenticator_Request_Find()..mergeFromMessage(this);
  Authenticator_Request_Find copyWith(void Function(Authenticator_Request_Find) updates) => super.copyWith((message) => updates(message as Authenticator_Request_Find));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Authenticator_Request_Find create() => Authenticator_Request_Find._();
  Authenticator_Request_Find createEmptyInstance() => create();
  static $pb.PbList<Authenticator_Request_Find> createRepeated() => $pb.PbList<Authenticator_Request_Find>();
  @$core.pragma('dart2js:noInline')
  static Authenticator_Request_Find getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Authenticator_Request_Find>(create);
  static Authenticator_Request_Find _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get id => $_getIZ(0);
  @$pb.TagNumber(1)
  set id($core.int v) { $_setUnsignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get name => $_getSZ(1);
  @$pb.TagNumber(2)
  set name($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasName() => $_has(1);
  @$pb.TagNumber(2)
  void clearName() => clearField(2);
}

class Authenticator_Request_Query extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Authenticator.Request.Query', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..aOS(1, 'filter')
    ..hasRequiredFields = false
  ;

  Authenticator_Request_Query._() : super();
  factory Authenticator_Request_Query() => create();
  factory Authenticator_Request_Query.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Authenticator_Request_Query.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Authenticator_Request_Query clone() => Authenticator_Request_Query()..mergeFromMessage(this);
  Authenticator_Request_Query copyWith(void Function(Authenticator_Request_Query) updates) => super.copyWith((message) => updates(message as Authenticator_Request_Query));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Authenticator_Request_Query create() => Authenticator_Request_Query._();
  Authenticator_Request_Query createEmptyInstance() => create();
  static $pb.PbList<Authenticator_Request_Query> createRepeated() => $pb.PbList<Authenticator_Request_Query>();
  @$core.pragma('dart2js:noInline')
  static Authenticator_Request_Query getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Authenticator_Request_Query>(create);
  static Authenticator_Request_Query _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get filter => $_getSZ(0);
  @$pb.TagNumber(1)
  set filter($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasFilter() => $_has(0);
  @$pb.TagNumber(1)
  void clearFilter() => clearField(1);
}

class Authenticator_Request_Register extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Authenticator.Request.Register', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..aOM<DatabaseUser>(1, 'user', subBuilder: DatabaseUser.create)
  ;

  Authenticator_Request_Register._() : super();
  factory Authenticator_Request_Register() => create();
  factory Authenticator_Request_Register.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Authenticator_Request_Register.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Authenticator_Request_Register clone() => Authenticator_Request_Register()..mergeFromMessage(this);
  Authenticator_Request_Register copyWith(void Function(Authenticator_Request_Register) updates) => super.copyWith((message) => updates(message as Authenticator_Request_Register));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Authenticator_Request_Register create() => Authenticator_Request_Register._();
  Authenticator_Request_Register createEmptyInstance() => create();
  static $pb.PbList<Authenticator_Request_Register> createRepeated() => $pb.PbList<Authenticator_Request_Register>();
  @$core.pragma('dart2js:noInline')
  static Authenticator_Request_Register getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Authenticator_Request_Register>(create);
  static Authenticator_Request_Register _defaultInstance;

  @$pb.TagNumber(1)
  DatabaseUser get user => $_getN(0);
  @$pb.TagNumber(1)
  set user(DatabaseUser v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasUser() => $_has(0);
  @$pb.TagNumber(1)
  void clearUser() => clearField(1);
  @$pb.TagNumber(1)
  DatabaseUser ensureUser() => $_ensure(0);
}

class Authenticator_Request_Deregister extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Authenticator.Request.Deregister', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..aOM<DatabaseUser>(1, 'user', subBuilder: DatabaseUser.create)
  ;

  Authenticator_Request_Deregister._() : super();
  factory Authenticator_Request_Deregister() => create();
  factory Authenticator_Request_Deregister.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Authenticator_Request_Deregister.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Authenticator_Request_Deregister clone() => Authenticator_Request_Deregister()..mergeFromMessage(this);
  Authenticator_Request_Deregister copyWith(void Function(Authenticator_Request_Deregister) updates) => super.copyWith((message) => updates(message as Authenticator_Request_Deregister));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Authenticator_Request_Deregister create() => Authenticator_Request_Deregister._();
  Authenticator_Request_Deregister createEmptyInstance() => create();
  static $pb.PbList<Authenticator_Request_Deregister> createRepeated() => $pb.PbList<Authenticator_Request_Deregister>();
  @$core.pragma('dart2js:noInline')
  static Authenticator_Request_Deregister getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Authenticator_Request_Deregister>(create);
  static Authenticator_Request_Deregister _defaultInstance;

  @$pb.TagNumber(1)
  DatabaseUser get user => $_getN(0);
  @$pb.TagNumber(1)
  set user(DatabaseUser v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasUser() => $_has(0);
  @$pb.TagNumber(1)
  void clearUser() => clearField(1);
  @$pb.TagNumber(1)
  DatabaseUser ensureUser() => $_ensure(0);
}

class Authenticator_Request_Update extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Authenticator.Request.Update', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..aOM<DatabaseUser>(1, 'user', subBuilder: DatabaseUser.create)
  ;

  Authenticator_Request_Update._() : super();
  factory Authenticator_Request_Update() => create();
  factory Authenticator_Request_Update.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Authenticator_Request_Update.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Authenticator_Request_Update clone() => Authenticator_Request_Update()..mergeFromMessage(this);
  Authenticator_Request_Update copyWith(void Function(Authenticator_Request_Update) updates) => super.copyWith((message) => updates(message as Authenticator_Request_Update));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Authenticator_Request_Update create() => Authenticator_Request_Update._();
  Authenticator_Request_Update createEmptyInstance() => create();
  static $pb.PbList<Authenticator_Request_Update> createRepeated() => $pb.PbList<Authenticator_Request_Update>();
  @$core.pragma('dart2js:noInline')
  static Authenticator_Request_Update getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Authenticator_Request_Update>(create);
  static Authenticator_Request_Update _defaultInstance;

  @$pb.TagNumber(1)
  DatabaseUser get user => $_getN(0);
  @$pb.TagNumber(1)
  set user(DatabaseUser v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasUser() => $_has(0);
  @$pb.TagNumber(1)
  void clearUser() => clearField(1);
  @$pb.TagNumber(1)
  DatabaseUser ensureUser() => $_ensure(0);
}

class Authenticator_Request extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Authenticator.Request', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..aOM<Authenticator_Request_Authenticate>(1, 'authenticate', subBuilder: Authenticator_Request_Authenticate.create)
    ..aOM<Authenticator_Request_Find>(2, 'find', subBuilder: Authenticator_Request_Find.create)
    ..aOM<Authenticator_Request_Query>(3, 'query', subBuilder: Authenticator_Request_Query.create)
    ..aOM<Authenticator_Request_Register>(4, 'register', subBuilder: Authenticator_Request_Register.create)
    ..aOM<Authenticator_Request_Deregister>(5, 'deregister', subBuilder: Authenticator_Request_Deregister.create)
    ..aOM<Authenticator_Request_Update>(6, 'update', subBuilder: Authenticator_Request_Update.create)
  ;

  Authenticator_Request._() : super();
  factory Authenticator_Request() => create();
  factory Authenticator_Request.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Authenticator_Request.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Authenticator_Request clone() => Authenticator_Request()..mergeFromMessage(this);
  Authenticator_Request copyWith(void Function(Authenticator_Request) updates) => super.copyWith((message) => updates(message as Authenticator_Request));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Authenticator_Request create() => Authenticator_Request._();
  Authenticator_Request createEmptyInstance() => create();
  static $pb.PbList<Authenticator_Request> createRepeated() => $pb.PbList<Authenticator_Request>();
  @$core.pragma('dart2js:noInline')
  static Authenticator_Request getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Authenticator_Request>(create);
  static Authenticator_Request _defaultInstance;

  @$pb.TagNumber(1)
  Authenticator_Request_Authenticate get authenticate => $_getN(0);
  @$pb.TagNumber(1)
  set authenticate(Authenticator_Request_Authenticate v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasAuthenticate() => $_has(0);
  @$pb.TagNumber(1)
  void clearAuthenticate() => clearField(1);
  @$pb.TagNumber(1)
  Authenticator_Request_Authenticate ensureAuthenticate() => $_ensure(0);

  @$pb.TagNumber(2)
  Authenticator_Request_Find get find => $_getN(1);
  @$pb.TagNumber(2)
  set find(Authenticator_Request_Find v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasFind() => $_has(1);
  @$pb.TagNumber(2)
  void clearFind() => clearField(2);
  @$pb.TagNumber(2)
  Authenticator_Request_Find ensureFind() => $_ensure(1);

  @$pb.TagNumber(3)
  Authenticator_Request_Query get query => $_getN(2);
  @$pb.TagNumber(3)
  set query(Authenticator_Request_Query v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasQuery() => $_has(2);
  @$pb.TagNumber(3)
  void clearQuery() => clearField(3);
  @$pb.TagNumber(3)
  Authenticator_Request_Query ensureQuery() => $_ensure(2);

  @$pb.TagNumber(4)
  Authenticator_Request_Register get register => $_getN(3);
  @$pb.TagNumber(4)
  set register(Authenticator_Request_Register v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasRegister() => $_has(3);
  @$pb.TagNumber(4)
  void clearRegister() => clearField(4);
  @$pb.TagNumber(4)
  Authenticator_Request_Register ensureRegister() => $_ensure(3);

  @$pb.TagNumber(5)
  Authenticator_Request_Deregister get deregister => $_getN(4);
  @$pb.TagNumber(5)
  set deregister(Authenticator_Request_Deregister v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasDeregister() => $_has(4);
  @$pb.TagNumber(5)
  void clearDeregister() => clearField(5);
  @$pb.TagNumber(5)
  Authenticator_Request_Deregister ensureDeregister() => $_ensure(4);

  @$pb.TagNumber(6)
  Authenticator_Request_Update get update => $_getN(5);
  @$pb.TagNumber(6)
  set update(Authenticator_Request_Update v) { setField(6, v); }
  @$pb.TagNumber(6)
  $core.bool hasUpdate() => $_has(5);
  @$pb.TagNumber(6)
  void clearUpdate() => clearField(6);
  @$pb.TagNumber(6)
  Authenticator_Request_Update ensureUpdate() => $_ensure(5);
}

class Authenticator_Response_Initialize extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Authenticator.Response.Initialize', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..aOM<Server>(1, 'server', subBuilder: Server.create)
  ;

  Authenticator_Response_Initialize._() : super();
  factory Authenticator_Response_Initialize() => create();
  factory Authenticator_Response_Initialize.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Authenticator_Response_Initialize.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Authenticator_Response_Initialize clone() => Authenticator_Response_Initialize()..mergeFromMessage(this);
  Authenticator_Response_Initialize copyWith(void Function(Authenticator_Response_Initialize) updates) => super.copyWith((message) => updates(message as Authenticator_Response_Initialize));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Authenticator_Response_Initialize create() => Authenticator_Response_Initialize._();
  Authenticator_Response_Initialize createEmptyInstance() => create();
  static $pb.PbList<Authenticator_Response_Initialize> createRepeated() => $pb.PbList<Authenticator_Response_Initialize>();
  @$core.pragma('dart2js:noInline')
  static Authenticator_Response_Initialize getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Authenticator_Response_Initialize>(create);
  static Authenticator_Response_Initialize _defaultInstance;

  @$pb.TagNumber(1)
  Server get server => $_getN(0);
  @$pb.TagNumber(1)
  set server(Server v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasServer() => $_has(0);
  @$pb.TagNumber(1)
  void clearServer() => clearField(1);
  @$pb.TagNumber(1)
  Server ensureServer() => $_ensure(0);
}

class Authenticator_Response_Authenticate extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Authenticator.Response.Authenticate', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..e<Authenticator_Response_Status>(1, 'status', $pb.PbFieldType.OE, defaultOrMaker: Authenticator_Response_Status.Fallthrough, valueOf: Authenticator_Response_Status.valueOf, enumValues: Authenticator_Response_Status.values)
    ..a<$core.int>(2, 'id', $pb.PbFieldType.OU3)
    ..aOS(3, 'name')
    ..pc<ACL_Group>(4, 'groups', $pb.PbFieldType.PM, subBuilder: ACL_Group.create)
  ;

  Authenticator_Response_Authenticate._() : super();
  factory Authenticator_Response_Authenticate() => create();
  factory Authenticator_Response_Authenticate.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Authenticator_Response_Authenticate.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Authenticator_Response_Authenticate clone() => Authenticator_Response_Authenticate()..mergeFromMessage(this);
  Authenticator_Response_Authenticate copyWith(void Function(Authenticator_Response_Authenticate) updates) => super.copyWith((message) => updates(message as Authenticator_Response_Authenticate));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Authenticator_Response_Authenticate create() => Authenticator_Response_Authenticate._();
  Authenticator_Response_Authenticate createEmptyInstance() => create();
  static $pb.PbList<Authenticator_Response_Authenticate> createRepeated() => $pb.PbList<Authenticator_Response_Authenticate>();
  @$core.pragma('dart2js:noInline')
  static Authenticator_Response_Authenticate getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Authenticator_Response_Authenticate>(create);
  static Authenticator_Response_Authenticate _defaultInstance;

  @$pb.TagNumber(1)
  Authenticator_Response_Status get status => $_getN(0);
  @$pb.TagNumber(1)
  set status(Authenticator_Response_Status v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasStatus() => $_has(0);
  @$pb.TagNumber(1)
  void clearStatus() => clearField(1);

  @$pb.TagNumber(2)
  $core.int get id => $_getIZ(1);
  @$pb.TagNumber(2)
  set id($core.int v) { $_setUnsignedInt32(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasId() => $_has(1);
  @$pb.TagNumber(2)
  void clearId() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get name => $_getSZ(2);
  @$pb.TagNumber(3)
  set name($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasName() => $_has(2);
  @$pb.TagNumber(3)
  void clearName() => clearField(3);

  @$pb.TagNumber(4)
  $core.List<ACL_Group> get groups => $_getList(3);
}

class Authenticator_Response_Find extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Authenticator.Response.Find', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..aOM<DatabaseUser>(1, 'user', subBuilder: DatabaseUser.create)
  ;

  Authenticator_Response_Find._() : super();
  factory Authenticator_Response_Find() => create();
  factory Authenticator_Response_Find.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Authenticator_Response_Find.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Authenticator_Response_Find clone() => Authenticator_Response_Find()..mergeFromMessage(this);
  Authenticator_Response_Find copyWith(void Function(Authenticator_Response_Find) updates) => super.copyWith((message) => updates(message as Authenticator_Response_Find));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Authenticator_Response_Find create() => Authenticator_Response_Find._();
  Authenticator_Response_Find createEmptyInstance() => create();
  static $pb.PbList<Authenticator_Response_Find> createRepeated() => $pb.PbList<Authenticator_Response_Find>();
  @$core.pragma('dart2js:noInline')
  static Authenticator_Response_Find getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Authenticator_Response_Find>(create);
  static Authenticator_Response_Find _defaultInstance;

  @$pb.TagNumber(1)
  DatabaseUser get user => $_getN(0);
  @$pb.TagNumber(1)
  set user(DatabaseUser v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasUser() => $_has(0);
  @$pb.TagNumber(1)
  void clearUser() => clearField(1);
  @$pb.TagNumber(1)
  DatabaseUser ensureUser() => $_ensure(0);
}

class Authenticator_Response_Query extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Authenticator.Response.Query', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..pc<DatabaseUser>(1, 'users', $pb.PbFieldType.PM, subBuilder: DatabaseUser.create)
  ;

  Authenticator_Response_Query._() : super();
  factory Authenticator_Response_Query() => create();
  factory Authenticator_Response_Query.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Authenticator_Response_Query.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Authenticator_Response_Query clone() => Authenticator_Response_Query()..mergeFromMessage(this);
  Authenticator_Response_Query copyWith(void Function(Authenticator_Response_Query) updates) => super.copyWith((message) => updates(message as Authenticator_Response_Query));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Authenticator_Response_Query create() => Authenticator_Response_Query._();
  Authenticator_Response_Query createEmptyInstance() => create();
  static $pb.PbList<Authenticator_Response_Query> createRepeated() => $pb.PbList<Authenticator_Response_Query>();
  @$core.pragma('dart2js:noInline')
  static Authenticator_Response_Query getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Authenticator_Response_Query>(create);
  static Authenticator_Response_Query _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<DatabaseUser> get users => $_getList(0);
}

class Authenticator_Response_Register extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Authenticator.Response.Register', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..e<Authenticator_Response_Status>(1, 'status', $pb.PbFieldType.OE, defaultOrMaker: Authenticator_Response_Status.Fallthrough, valueOf: Authenticator_Response_Status.valueOf, enumValues: Authenticator_Response_Status.values)
    ..aOM<DatabaseUser>(2, 'user', subBuilder: DatabaseUser.create)
  ;

  Authenticator_Response_Register._() : super();
  factory Authenticator_Response_Register() => create();
  factory Authenticator_Response_Register.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Authenticator_Response_Register.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Authenticator_Response_Register clone() => Authenticator_Response_Register()..mergeFromMessage(this);
  Authenticator_Response_Register copyWith(void Function(Authenticator_Response_Register) updates) => super.copyWith((message) => updates(message as Authenticator_Response_Register));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Authenticator_Response_Register create() => Authenticator_Response_Register._();
  Authenticator_Response_Register createEmptyInstance() => create();
  static $pb.PbList<Authenticator_Response_Register> createRepeated() => $pb.PbList<Authenticator_Response_Register>();
  @$core.pragma('dart2js:noInline')
  static Authenticator_Response_Register getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Authenticator_Response_Register>(create);
  static Authenticator_Response_Register _defaultInstance;

  @$pb.TagNumber(1)
  Authenticator_Response_Status get status => $_getN(0);
  @$pb.TagNumber(1)
  set status(Authenticator_Response_Status v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasStatus() => $_has(0);
  @$pb.TagNumber(1)
  void clearStatus() => clearField(1);

  @$pb.TagNumber(2)
  DatabaseUser get user => $_getN(1);
  @$pb.TagNumber(2)
  set user(DatabaseUser v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasUser() => $_has(1);
  @$pb.TagNumber(2)
  void clearUser() => clearField(2);
  @$pb.TagNumber(2)
  DatabaseUser ensureUser() => $_ensure(1);
}

class Authenticator_Response_Deregister extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Authenticator.Response.Deregister', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..e<Authenticator_Response_Status>(1, 'status', $pb.PbFieldType.OE, defaultOrMaker: Authenticator_Response_Status.Fallthrough, valueOf: Authenticator_Response_Status.valueOf, enumValues: Authenticator_Response_Status.values)
    ..hasRequiredFields = false
  ;

  Authenticator_Response_Deregister._() : super();
  factory Authenticator_Response_Deregister() => create();
  factory Authenticator_Response_Deregister.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Authenticator_Response_Deregister.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Authenticator_Response_Deregister clone() => Authenticator_Response_Deregister()..mergeFromMessage(this);
  Authenticator_Response_Deregister copyWith(void Function(Authenticator_Response_Deregister) updates) => super.copyWith((message) => updates(message as Authenticator_Response_Deregister));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Authenticator_Response_Deregister create() => Authenticator_Response_Deregister._();
  Authenticator_Response_Deregister createEmptyInstance() => create();
  static $pb.PbList<Authenticator_Response_Deregister> createRepeated() => $pb.PbList<Authenticator_Response_Deregister>();
  @$core.pragma('dart2js:noInline')
  static Authenticator_Response_Deregister getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Authenticator_Response_Deregister>(create);
  static Authenticator_Response_Deregister _defaultInstance;

  @$pb.TagNumber(1)
  Authenticator_Response_Status get status => $_getN(0);
  @$pb.TagNumber(1)
  set status(Authenticator_Response_Status v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasStatus() => $_has(0);
  @$pb.TagNumber(1)
  void clearStatus() => clearField(1);
}

class Authenticator_Response_Update extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Authenticator.Response.Update', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..e<Authenticator_Response_Status>(1, 'status', $pb.PbFieldType.OE, defaultOrMaker: Authenticator_Response_Status.Fallthrough, valueOf: Authenticator_Response_Status.valueOf, enumValues: Authenticator_Response_Status.values)
    ..hasRequiredFields = false
  ;

  Authenticator_Response_Update._() : super();
  factory Authenticator_Response_Update() => create();
  factory Authenticator_Response_Update.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Authenticator_Response_Update.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Authenticator_Response_Update clone() => Authenticator_Response_Update()..mergeFromMessage(this);
  Authenticator_Response_Update copyWith(void Function(Authenticator_Response_Update) updates) => super.copyWith((message) => updates(message as Authenticator_Response_Update));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Authenticator_Response_Update create() => Authenticator_Response_Update._();
  Authenticator_Response_Update createEmptyInstance() => create();
  static $pb.PbList<Authenticator_Response_Update> createRepeated() => $pb.PbList<Authenticator_Response_Update>();
  @$core.pragma('dart2js:noInline')
  static Authenticator_Response_Update getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Authenticator_Response_Update>(create);
  static Authenticator_Response_Update _defaultInstance;

  @$pb.TagNumber(1)
  Authenticator_Response_Status get status => $_getN(0);
  @$pb.TagNumber(1)
  set status(Authenticator_Response_Status v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasStatus() => $_has(0);
  @$pb.TagNumber(1)
  void clearStatus() => clearField(1);
}

class Authenticator_Response extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Authenticator.Response', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..aOM<Authenticator_Response_Initialize>(1, 'initialize', subBuilder: Authenticator_Response_Initialize.create)
    ..aOM<Authenticator_Response_Authenticate>(2, 'authenticate', subBuilder: Authenticator_Response_Authenticate.create)
    ..aOM<Authenticator_Response_Find>(3, 'find', subBuilder: Authenticator_Response_Find.create)
    ..aOM<Authenticator_Response_Query>(4, 'query', subBuilder: Authenticator_Response_Query.create)
    ..aOM<Authenticator_Response_Register>(5, 'register', subBuilder: Authenticator_Response_Register.create)
    ..aOM<Authenticator_Response_Deregister>(6, 'deregister', subBuilder: Authenticator_Response_Deregister.create)
    ..aOM<Authenticator_Response_Update>(7, 'update', subBuilder: Authenticator_Response_Update.create)
  ;

  Authenticator_Response._() : super();
  factory Authenticator_Response() => create();
  factory Authenticator_Response.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Authenticator_Response.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Authenticator_Response clone() => Authenticator_Response()..mergeFromMessage(this);
  Authenticator_Response copyWith(void Function(Authenticator_Response) updates) => super.copyWith((message) => updates(message as Authenticator_Response));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Authenticator_Response create() => Authenticator_Response._();
  Authenticator_Response createEmptyInstance() => create();
  static $pb.PbList<Authenticator_Response> createRepeated() => $pb.PbList<Authenticator_Response>();
  @$core.pragma('dart2js:noInline')
  static Authenticator_Response getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Authenticator_Response>(create);
  static Authenticator_Response _defaultInstance;

  @$pb.TagNumber(1)
  Authenticator_Response_Initialize get initialize => $_getN(0);
  @$pb.TagNumber(1)
  set initialize(Authenticator_Response_Initialize v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasInitialize() => $_has(0);
  @$pb.TagNumber(1)
  void clearInitialize() => clearField(1);
  @$pb.TagNumber(1)
  Authenticator_Response_Initialize ensureInitialize() => $_ensure(0);

  @$pb.TagNumber(2)
  Authenticator_Response_Authenticate get authenticate => $_getN(1);
  @$pb.TagNumber(2)
  set authenticate(Authenticator_Response_Authenticate v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasAuthenticate() => $_has(1);
  @$pb.TagNumber(2)
  void clearAuthenticate() => clearField(2);
  @$pb.TagNumber(2)
  Authenticator_Response_Authenticate ensureAuthenticate() => $_ensure(1);

  @$pb.TagNumber(3)
  Authenticator_Response_Find get find => $_getN(2);
  @$pb.TagNumber(3)
  set find(Authenticator_Response_Find v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasFind() => $_has(2);
  @$pb.TagNumber(3)
  void clearFind() => clearField(3);
  @$pb.TagNumber(3)
  Authenticator_Response_Find ensureFind() => $_ensure(2);

  @$pb.TagNumber(4)
  Authenticator_Response_Query get query => $_getN(3);
  @$pb.TagNumber(4)
  set query(Authenticator_Response_Query v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasQuery() => $_has(3);
  @$pb.TagNumber(4)
  void clearQuery() => clearField(4);
  @$pb.TagNumber(4)
  Authenticator_Response_Query ensureQuery() => $_ensure(3);

  @$pb.TagNumber(5)
  Authenticator_Response_Register get register => $_getN(4);
  @$pb.TagNumber(5)
  set register(Authenticator_Response_Register v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasRegister() => $_has(4);
  @$pb.TagNumber(5)
  void clearRegister() => clearField(5);
  @$pb.TagNumber(5)
  Authenticator_Response_Register ensureRegister() => $_ensure(4);

  @$pb.TagNumber(6)
  Authenticator_Response_Deregister get deregister => $_getN(5);
  @$pb.TagNumber(6)
  set deregister(Authenticator_Response_Deregister v) { setField(6, v); }
  @$pb.TagNumber(6)
  $core.bool hasDeregister() => $_has(5);
  @$pb.TagNumber(6)
  void clearDeregister() => clearField(6);
  @$pb.TagNumber(6)
  Authenticator_Response_Deregister ensureDeregister() => $_ensure(5);

  @$pb.TagNumber(7)
  Authenticator_Response_Update get update => $_getN(6);
  @$pb.TagNumber(7)
  set update(Authenticator_Response_Update v) { setField(7, v); }
  @$pb.TagNumber(7)
  $core.bool hasUpdate() => $_has(6);
  @$pb.TagNumber(7)
  void clearUpdate() => clearField(7);
  @$pb.TagNumber(7)
  Authenticator_Response_Update ensureUpdate() => $_ensure(6);
}

class Authenticator extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Authenticator', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  Authenticator._() : super();
  factory Authenticator() => create();
  factory Authenticator.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Authenticator.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Authenticator clone() => Authenticator()..mergeFromMessage(this);
  Authenticator copyWith(void Function(Authenticator) updates) => super.copyWith((message) => updates(message as Authenticator));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Authenticator create() => Authenticator._();
  Authenticator createEmptyInstance() => create();
  static $pb.PbList<Authenticator> createRepeated() => $pb.PbList<Authenticator>();
  @$core.pragma('dart2js:noInline')
  static Authenticator getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Authenticator>(create);
  static Authenticator _defaultInstance;
}

class DatabaseUser_Query extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('DatabaseUser.Query', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..aOM<Server>(1, 'server', subBuilder: Server.create)
    ..aOS(2, 'filter')
  ;

  DatabaseUser_Query._() : super();
  factory DatabaseUser_Query() => create();
  factory DatabaseUser_Query.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory DatabaseUser_Query.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  DatabaseUser_Query clone() => DatabaseUser_Query()..mergeFromMessage(this);
  DatabaseUser_Query copyWith(void Function(DatabaseUser_Query) updates) => super.copyWith((message) => updates(message as DatabaseUser_Query));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static DatabaseUser_Query create() => DatabaseUser_Query._();
  DatabaseUser_Query createEmptyInstance() => create();
  static $pb.PbList<DatabaseUser_Query> createRepeated() => $pb.PbList<DatabaseUser_Query>();
  @$core.pragma('dart2js:noInline')
  static DatabaseUser_Query getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<DatabaseUser_Query>(create);
  static DatabaseUser_Query _defaultInstance;

  @$pb.TagNumber(1)
  Server get server => $_getN(0);
  @$pb.TagNumber(1)
  set server(Server v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasServer() => $_has(0);
  @$pb.TagNumber(1)
  void clearServer() => clearField(1);
  @$pb.TagNumber(1)
  Server ensureServer() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.String get filter => $_getSZ(1);
  @$pb.TagNumber(2)
  set filter($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasFilter() => $_has(1);
  @$pb.TagNumber(2)
  void clearFilter() => clearField(2);
}

class DatabaseUser_List extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('DatabaseUser.List', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..aOM<Server>(1, 'server', subBuilder: Server.create)
    ..pc<DatabaseUser>(2, 'users', $pb.PbFieldType.PM, subBuilder: DatabaseUser.create)
  ;

  DatabaseUser_List._() : super();
  factory DatabaseUser_List() => create();
  factory DatabaseUser_List.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory DatabaseUser_List.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  DatabaseUser_List clone() => DatabaseUser_List()..mergeFromMessage(this);
  DatabaseUser_List copyWith(void Function(DatabaseUser_List) updates) => super.copyWith((message) => updates(message as DatabaseUser_List));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static DatabaseUser_List create() => DatabaseUser_List._();
  DatabaseUser_List createEmptyInstance() => create();
  static $pb.PbList<DatabaseUser_List> createRepeated() => $pb.PbList<DatabaseUser_List>();
  @$core.pragma('dart2js:noInline')
  static DatabaseUser_List getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<DatabaseUser_List>(create);
  static DatabaseUser_List _defaultInstance;

  @$pb.TagNumber(1)
  Server get server => $_getN(0);
  @$pb.TagNumber(1)
  set server(Server v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasServer() => $_has(0);
  @$pb.TagNumber(1)
  void clearServer() => clearField(1);
  @$pb.TagNumber(1)
  Server ensureServer() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.List<DatabaseUser> get users => $_getList(1);
}

class DatabaseUser_Verify extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('DatabaseUser.Verify', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..aOM<Server>(1, 'server', subBuilder: Server.create)
    ..aOS(2, 'name')
    ..aOS(3, 'password')
  ;

  DatabaseUser_Verify._() : super();
  factory DatabaseUser_Verify() => create();
  factory DatabaseUser_Verify.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory DatabaseUser_Verify.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  DatabaseUser_Verify clone() => DatabaseUser_Verify()..mergeFromMessage(this);
  DatabaseUser_Verify copyWith(void Function(DatabaseUser_Verify) updates) => super.copyWith((message) => updates(message as DatabaseUser_Verify));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static DatabaseUser_Verify create() => DatabaseUser_Verify._();
  DatabaseUser_Verify createEmptyInstance() => create();
  static $pb.PbList<DatabaseUser_Verify> createRepeated() => $pb.PbList<DatabaseUser_Verify>();
  @$core.pragma('dart2js:noInline')
  static DatabaseUser_Verify getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<DatabaseUser_Verify>(create);
  static DatabaseUser_Verify _defaultInstance;

  @$pb.TagNumber(1)
  Server get server => $_getN(0);
  @$pb.TagNumber(1)
  set server(Server v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasServer() => $_has(0);
  @$pb.TagNumber(1)
  void clearServer() => clearField(1);
  @$pb.TagNumber(1)
  Server ensureServer() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.String get name => $_getSZ(1);
  @$pb.TagNumber(2)
  set name($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasName() => $_has(1);
  @$pb.TagNumber(2)
  void clearName() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get password => $_getSZ(2);
  @$pb.TagNumber(3)
  set password($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasPassword() => $_has(2);
  @$pb.TagNumber(3)
  void clearPassword() => clearField(3);
}

class DatabaseUser extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('DatabaseUser', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..aOM<Server>(1, 'server', subBuilder: Server.create)
    ..a<$core.int>(2, 'id', $pb.PbFieldType.OU3)
    ..aOS(3, 'name')
    ..aOS(4, 'email')
    ..aOS(5, 'comment')
    ..aOS(6, 'hash')
    ..aOS(7, 'password')
    ..aOS(8, 'lastActive')
    ..a<$core.List<$core.int>>(9, 'texture', $pb.PbFieldType.OY)
  ;

  DatabaseUser._() : super();
  factory DatabaseUser() => create();
  factory DatabaseUser.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory DatabaseUser.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  DatabaseUser clone() => DatabaseUser()..mergeFromMessage(this);
  DatabaseUser copyWith(void Function(DatabaseUser) updates) => super.copyWith((message) => updates(message as DatabaseUser));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static DatabaseUser create() => DatabaseUser._();
  DatabaseUser createEmptyInstance() => create();
  static $pb.PbList<DatabaseUser> createRepeated() => $pb.PbList<DatabaseUser>();
  @$core.pragma('dart2js:noInline')
  static DatabaseUser getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<DatabaseUser>(create);
  static DatabaseUser _defaultInstance;

  @$pb.TagNumber(1)
  Server get server => $_getN(0);
  @$pb.TagNumber(1)
  set server(Server v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasServer() => $_has(0);
  @$pb.TagNumber(1)
  void clearServer() => clearField(1);
  @$pb.TagNumber(1)
  Server ensureServer() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.int get id => $_getIZ(1);
  @$pb.TagNumber(2)
  set id($core.int v) { $_setUnsignedInt32(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasId() => $_has(1);
  @$pb.TagNumber(2)
  void clearId() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get name => $_getSZ(2);
  @$pb.TagNumber(3)
  set name($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasName() => $_has(2);
  @$pb.TagNumber(3)
  void clearName() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get email => $_getSZ(3);
  @$pb.TagNumber(4)
  set email($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasEmail() => $_has(3);
  @$pb.TagNumber(4)
  void clearEmail() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get comment => $_getSZ(4);
  @$pb.TagNumber(5)
  set comment($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasComment() => $_has(4);
  @$pb.TagNumber(5)
  void clearComment() => clearField(5);

  @$pb.TagNumber(6)
  $core.String get hash => $_getSZ(5);
  @$pb.TagNumber(6)
  set hash($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasHash() => $_has(5);
  @$pb.TagNumber(6)
  void clearHash() => clearField(6);

  @$pb.TagNumber(7)
  $core.String get password => $_getSZ(6);
  @$pb.TagNumber(7)
  set password($core.String v) { $_setString(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasPassword() => $_has(6);
  @$pb.TagNumber(7)
  void clearPassword() => clearField(7);

  @$pb.TagNumber(8)
  $core.String get lastActive => $_getSZ(7);
  @$pb.TagNumber(8)
  set lastActive($core.String v) { $_setString(7, v); }
  @$pb.TagNumber(8)
  $core.bool hasLastActive() => $_has(7);
  @$pb.TagNumber(8)
  void clearLastActive() => clearField(8);

  @$pb.TagNumber(9)
  $core.List<$core.int> get texture => $_getN(8);
  @$pb.TagNumber(9)
  set texture($core.List<$core.int> v) { $_setBytes(8, v); }
  @$pb.TagNumber(9)
  $core.bool hasTexture() => $_has(8);
  @$pb.TagNumber(9)
  void clearTexture() => clearField(9);
}

class RedirectWhisperGroup extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('RedirectWhisperGroup', package: const $pb.PackageName('MurmurRPC'), createEmptyInstance: create)
    ..aOM<Server>(1, 'server', subBuilder: Server.create)
    ..aOM<User>(2, 'user', subBuilder: User.create)
    ..aOM<ACL_Group>(3, 'source', subBuilder: ACL_Group.create)
    ..aOM<ACL_Group>(4, 'target', subBuilder: ACL_Group.create)
  ;

  RedirectWhisperGroup._() : super();
  factory RedirectWhisperGroup() => create();
  factory RedirectWhisperGroup.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory RedirectWhisperGroup.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  RedirectWhisperGroup clone() => RedirectWhisperGroup()..mergeFromMessage(this);
  RedirectWhisperGroup copyWith(void Function(RedirectWhisperGroup) updates) => super.copyWith((message) => updates(message as RedirectWhisperGroup));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static RedirectWhisperGroup create() => RedirectWhisperGroup._();
  RedirectWhisperGroup createEmptyInstance() => create();
  static $pb.PbList<RedirectWhisperGroup> createRepeated() => $pb.PbList<RedirectWhisperGroup>();
  @$core.pragma('dart2js:noInline')
  static RedirectWhisperGroup getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<RedirectWhisperGroup>(create);
  static RedirectWhisperGroup _defaultInstance;

  @$pb.TagNumber(1)
  Server get server => $_getN(0);
  @$pb.TagNumber(1)
  set server(Server v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasServer() => $_has(0);
  @$pb.TagNumber(1)
  void clearServer() => clearField(1);
  @$pb.TagNumber(1)
  Server ensureServer() => $_ensure(0);

  @$pb.TagNumber(2)
  User get user => $_getN(1);
  @$pb.TagNumber(2)
  set user(User v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasUser() => $_has(1);
  @$pb.TagNumber(2)
  void clearUser() => clearField(2);
  @$pb.TagNumber(2)
  User ensureUser() => $_ensure(1);

  @$pb.TagNumber(3)
  ACL_Group get source => $_getN(2);
  @$pb.TagNumber(3)
  set source(ACL_Group v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasSource() => $_has(2);
  @$pb.TagNumber(3)
  void clearSource() => clearField(3);
  @$pb.TagNumber(3)
  ACL_Group ensureSource() => $_ensure(2);

  @$pb.TagNumber(4)
  ACL_Group get target => $_getN(3);
  @$pb.TagNumber(4)
  set target(ACL_Group v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasTarget() => $_has(3);
  @$pb.TagNumber(4)
  void clearTarget() => clearField(4);
  @$pb.TagNumber(4)
  ACL_Group ensureTarget() => $_ensure(3);
}

