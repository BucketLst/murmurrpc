///
//  Generated code. Do not modify.
//  source: murmurRPC.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

// ignore_for_file: UNDEFINED_SHOWN_NAME,UNUSED_SHOWN_NAME
import 'dart:core' as $core;
import 'package:protobuf/protobuf.dart' as $pb;

class Server_Event_Type extends $pb.ProtobufEnum {
  static const Server_Event_Type UserConnected = Server_Event_Type._(0, 'UserConnected');
  static const Server_Event_Type UserDisconnected = Server_Event_Type._(1, 'UserDisconnected');
  static const Server_Event_Type UserStateChanged = Server_Event_Type._(2, 'UserStateChanged');
  static const Server_Event_Type UserTextMessage = Server_Event_Type._(3, 'UserTextMessage');
  static const Server_Event_Type ChannelCreated = Server_Event_Type._(4, 'ChannelCreated');
  static const Server_Event_Type ChannelRemoved = Server_Event_Type._(5, 'ChannelRemoved');
  static const Server_Event_Type ChannelStateChanged = Server_Event_Type._(6, 'ChannelStateChanged');

  static const $core.List<Server_Event_Type> values = <Server_Event_Type> [
    UserConnected,
    UserDisconnected,
    UserStateChanged,
    UserTextMessage,
    ChannelCreated,
    ChannelRemoved,
    ChannelStateChanged,
  ];

  static final $core.Map<$core.int, Server_Event_Type> _byValue = $pb.ProtobufEnum.initByValue(values);
  static Server_Event_Type valueOf($core.int value) => _byValue[value];

  const Server_Event_Type._($core.int v, $core.String n) : super(v, n);
}

class Event_Type extends $pb.ProtobufEnum {
  static const Event_Type ServerStopped = Event_Type._(0, 'ServerStopped');
  static const Event_Type ServerStarted = Event_Type._(1, 'ServerStarted');

  static const $core.List<Event_Type> values = <Event_Type> [
    ServerStopped,
    ServerStarted,
  ];

  static final $core.Map<$core.int, Event_Type> _byValue = $pb.ProtobufEnum.initByValue(values);
  static Event_Type valueOf($core.int value) => _byValue[value];

  const Event_Type._($core.int v, $core.String n) : super(v, n);
}

class ContextAction_Context extends $pb.ProtobufEnum {
  static const ContextAction_Context Server = ContextAction_Context._(1, 'Server');
  static const ContextAction_Context Channel = ContextAction_Context._(2, 'Channel');
  static const ContextAction_Context User = ContextAction_Context._(4, 'User');

  static const $core.List<ContextAction_Context> values = <ContextAction_Context> [
    Server,
    Channel,
    User,
  ];

  static final $core.Map<$core.int, ContextAction_Context> _byValue = $pb.ProtobufEnum.initByValue(values);
  static ContextAction_Context valueOf($core.int value) => _byValue[value];

  const ContextAction_Context._($core.int v, $core.String n) : super(v, n);
}

class TextMessage_Filter_Action extends $pb.ProtobufEnum {
  static const TextMessage_Filter_Action Accept = TextMessage_Filter_Action._(0, 'Accept');
  static const TextMessage_Filter_Action Reject = TextMessage_Filter_Action._(1, 'Reject');
  static const TextMessage_Filter_Action Drop = TextMessage_Filter_Action._(2, 'Drop');

  static const $core.List<TextMessage_Filter_Action> values = <TextMessage_Filter_Action> [
    Accept,
    Reject,
    Drop,
  ];

  static final $core.Map<$core.int, TextMessage_Filter_Action> _byValue = $pb.ProtobufEnum.initByValue(values);
  static TextMessage_Filter_Action valueOf($core.int value) => _byValue[value];

  const TextMessage_Filter_Action._($core.int v, $core.String n) : super(v, n);
}

class ACL_Permission extends $pb.ProtobufEnum {
  static const ACL_Permission None = ACL_Permission._(0, 'None');
  static const ACL_Permission Write = ACL_Permission._(1, 'Write');
  static const ACL_Permission Traverse = ACL_Permission._(2, 'Traverse');
  static const ACL_Permission Enter = ACL_Permission._(4, 'Enter');
  static const ACL_Permission Speak = ACL_Permission._(8, 'Speak');
  static const ACL_Permission Whisper = ACL_Permission._(256, 'Whisper');
  static const ACL_Permission MuteDeafen = ACL_Permission._(16, 'MuteDeafen');
  static const ACL_Permission Move = ACL_Permission._(32, 'Move');
  static const ACL_Permission MakeChannel = ACL_Permission._(64, 'MakeChannel');
  static const ACL_Permission MakeTemporaryChannel = ACL_Permission._(1024, 'MakeTemporaryChannel');
  static const ACL_Permission LinkChannel = ACL_Permission._(128, 'LinkChannel');
  static const ACL_Permission TextMessage = ACL_Permission._(512, 'TextMessage');
  static const ACL_Permission Kick = ACL_Permission._(65536, 'Kick');
  static const ACL_Permission Ban = ACL_Permission._(131072, 'Ban');
  static const ACL_Permission Register = ACL_Permission._(262144, 'Register');
  static const ACL_Permission RegisterSelf = ACL_Permission._(524288, 'RegisterSelf');

  static const $core.List<ACL_Permission> values = <ACL_Permission> [
    None,
    Write,
    Traverse,
    Enter,
    Speak,
    Whisper,
    MuteDeafen,
    Move,
    MakeChannel,
    MakeTemporaryChannel,
    LinkChannel,
    TextMessage,
    Kick,
    Ban,
    Register,
    RegisterSelf,
  ];

  static final $core.Map<$core.int, ACL_Permission> _byValue = $pb.ProtobufEnum.initByValue(values);
  static ACL_Permission valueOf($core.int value) => _byValue[value];

  const ACL_Permission._($core.int v, $core.String n) : super(v, n);
}

class Authenticator_Response_Status extends $pb.ProtobufEnum {
  static const Authenticator_Response_Status Fallthrough = Authenticator_Response_Status._(0, 'Fallthrough');
  static const Authenticator_Response_Status Success = Authenticator_Response_Status._(1, 'Success');
  static const Authenticator_Response_Status Failure = Authenticator_Response_Status._(2, 'Failure');
  static const Authenticator_Response_Status TemporaryFailure = Authenticator_Response_Status._(3, 'TemporaryFailure');

  static const $core.List<Authenticator_Response_Status> values = <Authenticator_Response_Status> [
    Fallthrough,
    Success,
    Failure,
    TemporaryFailure,
  ];

  static final $core.Map<$core.int, Authenticator_Response_Status> _byValue = $pb.ProtobufEnum.initByValue(values);
  static Authenticator_Response_Status valueOf($core.int value) => _byValue[value];

  const Authenticator_Response_Status._($core.int v, $core.String n) : super(v, n);
}

