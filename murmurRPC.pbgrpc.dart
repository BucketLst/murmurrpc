///
//  Generated code. Do not modify.
//  source: murmurRPC.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'murmurRPC.pb.dart' as $0;
export 'murmurRPC.pb.dart';

class V1Client extends $grpc.Client {
  static final _$getUptime = $grpc.ClientMethod<$0.Void, $0.Uptime>(
      '/MurmurRPC.V1/GetUptime',
      ($0.Void value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Uptime.fromBuffer(value));
  static final _$getVersion = $grpc.ClientMethod<$0.Void, $0.Version>(
      '/MurmurRPC.V1/GetVersion',
      ($0.Void value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Version.fromBuffer(value));
  static final _$events = $grpc.ClientMethod<$0.Void, $0.Event>(
      '/MurmurRPC.V1/Events',
      ($0.Void value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Event.fromBuffer(value));
  static final _$serverCreate = $grpc.ClientMethod<$0.Void, $0.Server>(
      '/MurmurRPC.V1/ServerCreate',
      ($0.Void value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Server.fromBuffer(value));
  static final _$serverQuery =
      $grpc.ClientMethod<$0.Server_Query, $0.Server_List>(
          '/MurmurRPC.V1/ServerQuery',
          ($0.Server_Query value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.Server_List.fromBuffer(value));
  static final _$serverGet = $grpc.ClientMethod<$0.Server, $0.Server>(
      '/MurmurRPC.V1/ServerGet',
      ($0.Server value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Server.fromBuffer(value));
  static final _$serverStart = $grpc.ClientMethod<$0.Server, $0.Void>(
      '/MurmurRPC.V1/ServerStart',
      ($0.Server value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Void.fromBuffer(value));
  static final _$serverStop = $grpc.ClientMethod<$0.Server, $0.Void>(
      '/MurmurRPC.V1/ServerStop',
      ($0.Server value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Void.fromBuffer(value));
  static final _$serverRemove = $grpc.ClientMethod<$0.Server, $0.Void>(
      '/MurmurRPC.V1/ServerRemove',
      ($0.Server value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Void.fromBuffer(value));
  static final _$serverEvents = $grpc.ClientMethod<$0.Server, $0.Server_Event>(
      '/MurmurRPC.V1/ServerEvents',
      ($0.Server value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Server_Event.fromBuffer(value));
  static final _$contextActionAdd =
      $grpc.ClientMethod<$0.ContextAction, $0.Void>(
          '/MurmurRPC.V1/ContextActionAdd',
          ($0.ContextAction value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.Void.fromBuffer(value));
  static final _$contextActionRemove =
      $grpc.ClientMethod<$0.ContextAction, $0.Void>(
          '/MurmurRPC.V1/ContextActionRemove',
          ($0.ContextAction value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.Void.fromBuffer(value));
  static final _$contextActionEvents =
      $grpc.ClientMethod<$0.ContextAction, $0.ContextAction>(
          '/MurmurRPC.V1/ContextActionEvents',
          ($0.ContextAction value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.ContextAction.fromBuffer(value));
  static final _$textMessageSend = $grpc.ClientMethod<$0.TextMessage, $0.Void>(
      '/MurmurRPC.V1/TextMessageSend',
      ($0.TextMessage value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Void.fromBuffer(value));
  static final _$textMessageFilter =
      $grpc.ClientMethod<$0.TextMessage_Filter, $0.TextMessage_Filter>(
          '/MurmurRPC.V1/TextMessageFilter',
          ($0.TextMessage_Filter value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.TextMessage_Filter.fromBuffer(value));
  static final _$logQuery = $grpc.ClientMethod<$0.Log_Query, $0.Log_List>(
      '/MurmurRPC.V1/LogQuery',
      ($0.Log_Query value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Log_List.fromBuffer(value));
  static final _$configGet = $grpc.ClientMethod<$0.Server, $0.Config>(
      '/MurmurRPC.V1/ConfigGet',
      ($0.Server value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Config.fromBuffer(value));
  static final _$configGetField =
      $grpc.ClientMethod<$0.Config_Field, $0.Config_Field>(
          '/MurmurRPC.V1/ConfigGetField',
          ($0.Config_Field value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.Config_Field.fromBuffer(value));
  static final _$configSetField = $grpc.ClientMethod<$0.Config_Field, $0.Void>(
      '/MurmurRPC.V1/ConfigSetField',
      ($0.Config_Field value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Void.fromBuffer(value));
  static final _$configGetDefault = $grpc.ClientMethod<$0.Void, $0.Config>(
      '/MurmurRPC.V1/ConfigGetDefault',
      ($0.Void value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Config.fromBuffer(value));
  static final _$channelQuery =
      $grpc.ClientMethod<$0.Channel_Query, $0.Channel_List>(
          '/MurmurRPC.V1/ChannelQuery',
          ($0.Channel_Query value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.Channel_List.fromBuffer(value));
  static final _$channelGet = $grpc.ClientMethod<$0.Channel, $0.Channel>(
      '/MurmurRPC.V1/ChannelGet',
      ($0.Channel value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Channel.fromBuffer(value));
  static final _$channelAdd = $grpc.ClientMethod<$0.Channel, $0.Channel>(
      '/MurmurRPC.V1/ChannelAdd',
      ($0.Channel value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Channel.fromBuffer(value));
  static final _$channelRemove = $grpc.ClientMethod<$0.Channel, $0.Void>(
      '/MurmurRPC.V1/ChannelRemove',
      ($0.Channel value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Void.fromBuffer(value));
  static final _$channelUpdate = $grpc.ClientMethod<$0.Channel, $0.Channel>(
      '/MurmurRPC.V1/ChannelUpdate',
      ($0.Channel value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Channel.fromBuffer(value));
  static final _$userQuery = $grpc.ClientMethod<$0.User_Query, $0.User_List>(
      '/MurmurRPC.V1/UserQuery',
      ($0.User_Query value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.User_List.fromBuffer(value));
  static final _$userGet = $grpc.ClientMethod<$0.User, $0.User>(
      '/MurmurRPC.V1/UserGet',
      ($0.User value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.User.fromBuffer(value));
  static final _$userUpdate = $grpc.ClientMethod<$0.User, $0.User>(
      '/MurmurRPC.V1/UserUpdate',
      ($0.User value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.User.fromBuffer(value));
  static final _$userKick = $grpc.ClientMethod<$0.User_Kick, $0.Void>(
      '/MurmurRPC.V1/UserKick',
      ($0.User_Kick value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Void.fromBuffer(value));
  static final _$treeQuery = $grpc.ClientMethod<$0.Tree_Query, $0.Tree>(
      '/MurmurRPC.V1/TreeQuery',
      ($0.Tree_Query value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Tree.fromBuffer(value));
  static final _$bansGet = $grpc.ClientMethod<$0.Ban_Query, $0.Ban_List>(
      '/MurmurRPC.V1/BansGet',
      ($0.Ban_Query value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Ban_List.fromBuffer(value));
  static final _$bansSet = $grpc.ClientMethod<$0.Ban_List, $0.Void>(
      '/MurmurRPC.V1/BansSet',
      ($0.Ban_List value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Void.fromBuffer(value));
  static final _$aCLGet = $grpc.ClientMethod<$0.Channel, $0.ACL_List>(
      '/MurmurRPC.V1/ACLGet',
      ($0.Channel value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.ACL_List.fromBuffer(value));
  static final _$aCLSet = $grpc.ClientMethod<$0.ACL_List, $0.Void>(
      '/MurmurRPC.V1/ACLSet',
      ($0.ACL_List value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Void.fromBuffer(value));
  static final _$aCLGetEffectivePermissions =
      $grpc.ClientMethod<$0.ACL_Query, $0.ACL>(
          '/MurmurRPC.V1/ACLGetEffectivePermissions',
          ($0.ACL_Query value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.ACL.fromBuffer(value));
  static final _$aCLAddTemporaryGroup =
      $grpc.ClientMethod<$0.ACL_TemporaryGroup, $0.Void>(
          '/MurmurRPC.V1/ACLAddTemporaryGroup',
          ($0.ACL_TemporaryGroup value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.Void.fromBuffer(value));
  static final _$aCLRemoveTemporaryGroup =
      $grpc.ClientMethod<$0.ACL_TemporaryGroup, $0.Void>(
          '/MurmurRPC.V1/ACLRemoveTemporaryGroup',
          ($0.ACL_TemporaryGroup value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.Void.fromBuffer(value));
  static final _$authenticatorStream =
      $grpc.ClientMethod<$0.Authenticator_Response, $0.Authenticator_Request>(
          '/MurmurRPC.V1/AuthenticatorStream',
          ($0.Authenticator_Response value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.Authenticator_Request.fromBuffer(value));
  static final _$databaseUserQuery =
      $grpc.ClientMethod<$0.DatabaseUser_Query, $0.DatabaseUser_List>(
          '/MurmurRPC.V1/DatabaseUserQuery',
          ($0.DatabaseUser_Query value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.DatabaseUser_List.fromBuffer(value));
  static final _$databaseUserGet =
      $grpc.ClientMethod<$0.DatabaseUser, $0.DatabaseUser>(
          '/MurmurRPC.V1/DatabaseUserGet',
          ($0.DatabaseUser value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.DatabaseUser.fromBuffer(value));
  static final _$databaseUserUpdate =
      $grpc.ClientMethod<$0.DatabaseUser, $0.Void>(
          '/MurmurRPC.V1/DatabaseUserUpdate',
          ($0.DatabaseUser value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.Void.fromBuffer(value));
  static final _$databaseUserRegister =
      $grpc.ClientMethod<$0.DatabaseUser, $0.DatabaseUser>(
          '/MurmurRPC.V1/DatabaseUserRegister',
          ($0.DatabaseUser value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.DatabaseUser.fromBuffer(value));
  static final _$databaseUserDeregister =
      $grpc.ClientMethod<$0.DatabaseUser, $0.Void>(
          '/MurmurRPC.V1/DatabaseUserDeregister',
          ($0.DatabaseUser value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.Void.fromBuffer(value));
  static final _$databaseUserVerify =
      $grpc.ClientMethod<$0.DatabaseUser_Verify, $0.DatabaseUser>(
          '/MurmurRPC.V1/DatabaseUserVerify',
          ($0.DatabaseUser_Verify value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.DatabaseUser.fromBuffer(value));
  static final _$redirectWhisperGroupAdd =
      $grpc.ClientMethod<$0.RedirectWhisperGroup, $0.Void>(
          '/MurmurRPC.V1/RedirectWhisperGroupAdd',
          ($0.RedirectWhisperGroup value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.Void.fromBuffer(value));
  static final _$redirectWhisperGroupRemove =
      $grpc.ClientMethod<$0.RedirectWhisperGroup, $0.Void>(
          '/MurmurRPC.V1/RedirectWhisperGroupRemove',
          ($0.RedirectWhisperGroup value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.Void.fromBuffer(value));

  V1Client($grpc.ClientChannel channel, {$grpc.CallOptions options})
      : super(channel, options: options);

  $grpc.ResponseFuture<$0.Uptime> getUptime($0.Void request,
      {$grpc.CallOptions options}) {
    final call = $createCall(_$getUptime, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.Version> getVersion($0.Void request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$getVersion, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseStream<$0.Event> events($0.Void request,
      {$grpc.CallOptions options}) {
    final call = $createCall(_$events, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseStream(call);
  }

  $grpc.ResponseFuture<$0.Server> serverCreate($0.Void request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$serverCreate, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.Server_List> serverQuery($0.Server_Query request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$serverQuery, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.Server> serverGet($0.Server request,
      {$grpc.CallOptions options}) {
    final call = $createCall(_$serverGet, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.Void> serverStart($0.Server request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$serverStart, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.Void> serverStop($0.Server request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$serverStop, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.Void> serverRemove($0.Server request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$serverRemove, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseStream<$0.Server_Event> serverEvents($0.Server request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$serverEvents, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseStream(call);
  }

  $grpc.ResponseFuture<$0.Void> contextActionAdd($0.ContextAction request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$contextActionAdd, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.Void> contextActionRemove($0.ContextAction request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$contextActionRemove, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseStream<$0.ContextAction> contextActionEvents(
      $0.ContextAction request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$contextActionEvents, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseStream(call);
  }

  $grpc.ResponseFuture<$0.Void> textMessageSend($0.TextMessage request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$textMessageSend, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseStream<$0.TextMessage_Filter> textMessageFilter(
      $async.Stream<$0.TextMessage_Filter> request,
      {$grpc.CallOptions options}) {
    final call = $createCall(_$textMessageFilter, request, options: options);
    return $grpc.ResponseStream(call);
  }

  $grpc.ResponseFuture<$0.Log_List> logQuery($0.Log_Query request,
      {$grpc.CallOptions options}) {
    final call = $createCall(_$logQuery, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.Config> configGet($0.Server request,
      {$grpc.CallOptions options}) {
    final call = $createCall(_$configGet, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.Config_Field> configGetField($0.Config_Field request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$configGetField, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.Void> configSetField($0.Config_Field request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$configSetField, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.Config> configGetDefault($0.Void request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$configGetDefault, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.Channel_List> channelQuery($0.Channel_Query request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$channelQuery, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.Channel> channelGet($0.Channel request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$channelGet, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.Channel> channelAdd($0.Channel request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$channelAdd, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.Void> channelRemove($0.Channel request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$channelRemove, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.Channel> channelUpdate($0.Channel request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$channelUpdate, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.User_List> userQuery($0.User_Query request,
      {$grpc.CallOptions options}) {
    final call = $createCall(_$userQuery, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.User> userGet($0.User request,
      {$grpc.CallOptions options}) {
    final call = $createCall(_$userGet, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.User> userUpdate($0.User request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$userUpdate, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.Void> userKick($0.User_Kick request,
      {$grpc.CallOptions options}) {
    final call = $createCall(_$userKick, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.Tree> treeQuery($0.Tree_Query request,
      {$grpc.CallOptions options}) {
    final call = $createCall(_$treeQuery, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.Ban_List> bansGet($0.Ban_Query request,
      {$grpc.CallOptions options}) {
    final call = $createCall(_$bansGet, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.Void> bansSet($0.Ban_List request,
      {$grpc.CallOptions options}) {
    final call = $createCall(_$bansSet, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.ACL_List> aCLGet($0.Channel request,
      {$grpc.CallOptions options}) {
    final call = $createCall(_$aCLGet, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.Void> aCLSet($0.ACL_List request,
      {$grpc.CallOptions options}) {
    final call = $createCall(_$aCLSet, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.ACL> aCLGetEffectivePermissions($0.ACL_Query request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$aCLGetEffectivePermissions, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.Void> aCLAddTemporaryGroup(
      $0.ACL_TemporaryGroup request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$aCLAddTemporaryGroup, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.Void> aCLRemoveTemporaryGroup(
      $0.ACL_TemporaryGroup request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$aCLRemoveTemporaryGroup, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseStream<$0.Authenticator_Request> authenticatorStream(
      $async.Stream<$0.Authenticator_Response> request,
      {$grpc.CallOptions options}) {
    final call = $createCall(_$authenticatorStream, request, options: options);
    return $grpc.ResponseStream(call);
  }

  $grpc.ResponseFuture<$0.DatabaseUser_List> databaseUserQuery(
      $0.DatabaseUser_Query request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$databaseUserQuery, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.DatabaseUser> databaseUserGet($0.DatabaseUser request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$databaseUserGet, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.Void> databaseUserUpdate($0.DatabaseUser request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$databaseUserUpdate, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.DatabaseUser> databaseUserRegister(
      $0.DatabaseUser request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$databaseUserRegister, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.Void> databaseUserDeregister($0.DatabaseUser request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$databaseUserDeregister, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.DatabaseUser> databaseUserVerify(
      $0.DatabaseUser_Verify request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$databaseUserVerify, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.Void> redirectWhisperGroupAdd(
      $0.RedirectWhisperGroup request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$redirectWhisperGroupAdd, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.Void> redirectWhisperGroupRemove(
      $0.RedirectWhisperGroup request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$redirectWhisperGroupRemove, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }
}

abstract class V1ServiceBase extends $grpc.Service {
  $core.String get $name => 'MurmurRPC.V1';

  V1ServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.Void, $0.Uptime>(
        'GetUptime',
        getUptime_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Void.fromBuffer(value),
        ($0.Uptime value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Void, $0.Version>(
        'GetVersion',
        getVersion_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Void.fromBuffer(value),
        ($0.Version value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Void, $0.Event>(
        'Events',
        events_Pre,
        false,
        true,
        ($core.List<$core.int> value) => $0.Void.fromBuffer(value),
        ($0.Event value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Void, $0.Server>(
        'ServerCreate',
        serverCreate_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Void.fromBuffer(value),
        ($0.Server value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Server_Query, $0.Server_List>(
        'ServerQuery',
        serverQuery_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Server_Query.fromBuffer(value),
        ($0.Server_List value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Server, $0.Server>(
        'ServerGet',
        serverGet_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Server.fromBuffer(value),
        ($0.Server value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Server, $0.Void>(
        'ServerStart',
        serverStart_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Server.fromBuffer(value),
        ($0.Void value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Server, $0.Void>(
        'ServerStop',
        serverStop_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Server.fromBuffer(value),
        ($0.Void value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Server, $0.Void>(
        'ServerRemove',
        serverRemove_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Server.fromBuffer(value),
        ($0.Void value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Server, $0.Server_Event>(
        'ServerEvents',
        serverEvents_Pre,
        false,
        true,
        ($core.List<$core.int> value) => $0.Server.fromBuffer(value),
        ($0.Server_Event value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.ContextAction, $0.Void>(
        'ContextActionAdd',
        contextActionAdd_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.ContextAction.fromBuffer(value),
        ($0.Void value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.ContextAction, $0.Void>(
        'ContextActionRemove',
        contextActionRemove_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.ContextAction.fromBuffer(value),
        ($0.Void value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.ContextAction, $0.ContextAction>(
        'ContextActionEvents',
        contextActionEvents_Pre,
        false,
        true,
        ($core.List<$core.int> value) => $0.ContextAction.fromBuffer(value),
        ($0.ContextAction value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.TextMessage, $0.Void>(
        'TextMessageSend',
        textMessageSend_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.TextMessage.fromBuffer(value),
        ($0.Void value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$0.TextMessage_Filter, $0.TextMessage_Filter>(
            'TextMessageFilter',
            textMessageFilter,
            true,
            true,
            ($core.List<$core.int> value) =>
                $0.TextMessage_Filter.fromBuffer(value),
            ($0.TextMessage_Filter value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Log_Query, $0.Log_List>(
        'LogQuery',
        logQuery_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Log_Query.fromBuffer(value),
        ($0.Log_List value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Server, $0.Config>(
        'ConfigGet',
        configGet_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Server.fromBuffer(value),
        ($0.Config value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Config_Field, $0.Config_Field>(
        'ConfigGetField',
        configGetField_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Config_Field.fromBuffer(value),
        ($0.Config_Field value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Config_Field, $0.Void>(
        'ConfigSetField',
        configSetField_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Config_Field.fromBuffer(value),
        ($0.Void value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Void, $0.Config>(
        'ConfigGetDefault',
        configGetDefault_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Void.fromBuffer(value),
        ($0.Config value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Channel_Query, $0.Channel_List>(
        'ChannelQuery',
        channelQuery_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Channel_Query.fromBuffer(value),
        ($0.Channel_List value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Channel, $0.Channel>(
        'ChannelGet',
        channelGet_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Channel.fromBuffer(value),
        ($0.Channel value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Channel, $0.Channel>(
        'ChannelAdd',
        channelAdd_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Channel.fromBuffer(value),
        ($0.Channel value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Channel, $0.Void>(
        'ChannelRemove',
        channelRemove_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Channel.fromBuffer(value),
        ($0.Void value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Channel, $0.Channel>(
        'ChannelUpdate',
        channelUpdate_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Channel.fromBuffer(value),
        ($0.Channel value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.User_Query, $0.User_List>(
        'UserQuery',
        userQuery_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.User_Query.fromBuffer(value),
        ($0.User_List value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.User, $0.User>(
        'UserGet',
        userGet_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.User.fromBuffer(value),
        ($0.User value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.User, $0.User>(
        'UserUpdate',
        userUpdate_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.User.fromBuffer(value),
        ($0.User value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.User_Kick, $0.Void>(
        'UserKick',
        userKick_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.User_Kick.fromBuffer(value),
        ($0.Void value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Tree_Query, $0.Tree>(
        'TreeQuery',
        treeQuery_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Tree_Query.fromBuffer(value),
        ($0.Tree value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Ban_Query, $0.Ban_List>(
        'BansGet',
        bansGet_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Ban_Query.fromBuffer(value),
        ($0.Ban_List value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Ban_List, $0.Void>(
        'BansSet',
        bansSet_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Ban_List.fromBuffer(value),
        ($0.Void value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Channel, $0.ACL_List>(
        'ACLGet',
        aCLGet_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Channel.fromBuffer(value),
        ($0.ACL_List value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.ACL_List, $0.Void>(
        'ACLSet',
        aCLSet_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.ACL_List.fromBuffer(value),
        ($0.Void value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.ACL_Query, $0.ACL>(
        'ACLGetEffectivePermissions',
        aCLGetEffectivePermissions_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.ACL_Query.fromBuffer(value),
        ($0.ACL value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.ACL_TemporaryGroup, $0.Void>(
        'ACLAddTemporaryGroup',
        aCLAddTemporaryGroup_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.ACL_TemporaryGroup.fromBuffer(value),
        ($0.Void value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.ACL_TemporaryGroup, $0.Void>(
        'ACLRemoveTemporaryGroup',
        aCLRemoveTemporaryGroup_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.ACL_TemporaryGroup.fromBuffer(value),
        ($0.Void value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Authenticator_Response,
            $0.Authenticator_Request>(
        'AuthenticatorStream',
        authenticatorStream,
        true,
        true,
        ($core.List<$core.int> value) =>
            $0.Authenticator_Response.fromBuffer(value),
        ($0.Authenticator_Request value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.DatabaseUser_Query, $0.DatabaseUser_List>(
        'DatabaseUserQuery',
        databaseUserQuery_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.DatabaseUser_Query.fromBuffer(value),
        ($0.DatabaseUser_List value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.DatabaseUser, $0.DatabaseUser>(
        'DatabaseUserGet',
        databaseUserGet_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.DatabaseUser.fromBuffer(value),
        ($0.DatabaseUser value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.DatabaseUser, $0.Void>(
        'DatabaseUserUpdate',
        databaseUserUpdate_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.DatabaseUser.fromBuffer(value),
        ($0.Void value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.DatabaseUser, $0.DatabaseUser>(
        'DatabaseUserRegister',
        databaseUserRegister_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.DatabaseUser.fromBuffer(value),
        ($0.DatabaseUser value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.DatabaseUser, $0.Void>(
        'DatabaseUserDeregister',
        databaseUserDeregister_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.DatabaseUser.fromBuffer(value),
        ($0.Void value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.DatabaseUser_Verify, $0.DatabaseUser>(
        'DatabaseUserVerify',
        databaseUserVerify_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.DatabaseUser_Verify.fromBuffer(value),
        ($0.DatabaseUser value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.RedirectWhisperGroup, $0.Void>(
        'RedirectWhisperGroupAdd',
        redirectWhisperGroupAdd_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.RedirectWhisperGroup.fromBuffer(value),
        ($0.Void value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.RedirectWhisperGroup, $0.Void>(
        'RedirectWhisperGroupRemove',
        redirectWhisperGroupRemove_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.RedirectWhisperGroup.fromBuffer(value),
        ($0.Void value) => value.writeToBuffer()));
  }

  $async.Future<$0.Uptime> getUptime_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Void> request) async {
    return getUptime(call, await request);
  }

  $async.Future<$0.Version> getVersion_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Void> request) async {
    return getVersion(call, await request);
  }

  $async.Stream<$0.Event> events_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Void> request) async* {
    yield* events(call, await request);
  }

  $async.Future<$0.Server> serverCreate_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Void> request) async {
    return serverCreate(call, await request);
  }

  $async.Future<$0.Server_List> serverQuery_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Server_Query> request) async {
    return serverQuery(call, await request);
  }

  $async.Future<$0.Server> serverGet_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Server> request) async {
    return serverGet(call, await request);
  }

  $async.Future<$0.Void> serverStart_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Server> request) async {
    return serverStart(call, await request);
  }

  $async.Future<$0.Void> serverStop_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Server> request) async {
    return serverStop(call, await request);
  }

  $async.Future<$0.Void> serverRemove_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Server> request) async {
    return serverRemove(call, await request);
  }

  $async.Stream<$0.Server_Event> serverEvents_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Server> request) async* {
    yield* serverEvents(call, await request);
  }

  $async.Future<$0.Void> contextActionAdd_Pre(
      $grpc.ServiceCall call, $async.Future<$0.ContextAction> request) async {
    return contextActionAdd(call, await request);
  }

  $async.Future<$0.Void> contextActionRemove_Pre(
      $grpc.ServiceCall call, $async.Future<$0.ContextAction> request) async {
    return contextActionRemove(call, await request);
  }

  $async.Stream<$0.ContextAction> contextActionEvents_Pre(
      $grpc.ServiceCall call, $async.Future<$0.ContextAction> request) async* {
    yield* contextActionEvents(call, await request);
  }

  $async.Future<$0.Void> textMessageSend_Pre(
      $grpc.ServiceCall call, $async.Future<$0.TextMessage> request) async {
    return textMessageSend(call, await request);
  }

  $async.Future<$0.Log_List> logQuery_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Log_Query> request) async {
    return logQuery(call, await request);
  }

  $async.Future<$0.Config> configGet_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Server> request) async {
    return configGet(call, await request);
  }

  $async.Future<$0.Config_Field> configGetField_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Config_Field> request) async {
    return configGetField(call, await request);
  }

  $async.Future<$0.Void> configSetField_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Config_Field> request) async {
    return configSetField(call, await request);
  }

  $async.Future<$0.Config> configGetDefault_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Void> request) async {
    return configGetDefault(call, await request);
  }

  $async.Future<$0.Channel_List> channelQuery_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Channel_Query> request) async {
    return channelQuery(call, await request);
  }

  $async.Future<$0.Channel> channelGet_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Channel> request) async {
    return channelGet(call, await request);
  }

  $async.Future<$0.Channel> channelAdd_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Channel> request) async {
    return channelAdd(call, await request);
  }

  $async.Future<$0.Void> channelRemove_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Channel> request) async {
    return channelRemove(call, await request);
  }

  $async.Future<$0.Channel> channelUpdate_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Channel> request) async {
    return channelUpdate(call, await request);
  }

  $async.Future<$0.User_List> userQuery_Pre(
      $grpc.ServiceCall call, $async.Future<$0.User_Query> request) async {
    return userQuery(call, await request);
  }

  $async.Future<$0.User> userGet_Pre(
      $grpc.ServiceCall call, $async.Future<$0.User> request) async {
    return userGet(call, await request);
  }

  $async.Future<$0.User> userUpdate_Pre(
      $grpc.ServiceCall call, $async.Future<$0.User> request) async {
    return userUpdate(call, await request);
  }

  $async.Future<$0.Void> userKick_Pre(
      $grpc.ServiceCall call, $async.Future<$0.User_Kick> request) async {
    return userKick(call, await request);
  }

  $async.Future<$0.Tree> treeQuery_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Tree_Query> request) async {
    return treeQuery(call, await request);
  }

  $async.Future<$0.Ban_List> bansGet_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Ban_Query> request) async {
    return bansGet(call, await request);
  }

  $async.Future<$0.Void> bansSet_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Ban_List> request) async {
    return bansSet(call, await request);
  }

  $async.Future<$0.ACL_List> aCLGet_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Channel> request) async {
    return aCLGet(call, await request);
  }

  $async.Future<$0.Void> aCLSet_Pre(
      $grpc.ServiceCall call, $async.Future<$0.ACL_List> request) async {
    return aCLSet(call, await request);
  }

  $async.Future<$0.ACL> aCLGetEffectivePermissions_Pre(
      $grpc.ServiceCall call, $async.Future<$0.ACL_Query> request) async {
    return aCLGetEffectivePermissions(call, await request);
  }

  $async.Future<$0.Void> aCLAddTemporaryGroup_Pre($grpc.ServiceCall call,
      $async.Future<$0.ACL_TemporaryGroup> request) async {
    return aCLAddTemporaryGroup(call, await request);
  }

  $async.Future<$0.Void> aCLRemoveTemporaryGroup_Pre($grpc.ServiceCall call,
      $async.Future<$0.ACL_TemporaryGroup> request) async {
    return aCLRemoveTemporaryGroup(call, await request);
  }

  $async.Future<$0.DatabaseUser_List> databaseUserQuery_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.DatabaseUser_Query> request) async {
    return databaseUserQuery(call, await request);
  }

  $async.Future<$0.DatabaseUser> databaseUserGet_Pre(
      $grpc.ServiceCall call, $async.Future<$0.DatabaseUser> request) async {
    return databaseUserGet(call, await request);
  }

  $async.Future<$0.Void> databaseUserUpdate_Pre(
      $grpc.ServiceCall call, $async.Future<$0.DatabaseUser> request) async {
    return databaseUserUpdate(call, await request);
  }

  $async.Future<$0.DatabaseUser> databaseUserRegister_Pre(
      $grpc.ServiceCall call, $async.Future<$0.DatabaseUser> request) async {
    return databaseUserRegister(call, await request);
  }

  $async.Future<$0.Void> databaseUserDeregister_Pre(
      $grpc.ServiceCall call, $async.Future<$0.DatabaseUser> request) async {
    return databaseUserDeregister(call, await request);
  }

  $async.Future<$0.DatabaseUser> databaseUserVerify_Pre($grpc.ServiceCall call,
      $async.Future<$0.DatabaseUser_Verify> request) async {
    return databaseUserVerify(call, await request);
  }

  $async.Future<$0.Void> redirectWhisperGroupAdd_Pre($grpc.ServiceCall call,
      $async.Future<$0.RedirectWhisperGroup> request) async {
    return redirectWhisperGroupAdd(call, await request);
  }

  $async.Future<$0.Void> redirectWhisperGroupRemove_Pre($grpc.ServiceCall call,
      $async.Future<$0.RedirectWhisperGroup> request) async {
    return redirectWhisperGroupRemove(call, await request);
  }

  $async.Future<$0.Uptime> getUptime($grpc.ServiceCall call, $0.Void request);
  $async.Future<$0.Version> getVersion($grpc.ServiceCall call, $0.Void request);
  $async.Stream<$0.Event> events($grpc.ServiceCall call, $0.Void request);
  $async.Future<$0.Server> serverCreate(
      $grpc.ServiceCall call, $0.Void request);
  $async.Future<$0.Server_List> serverQuery(
      $grpc.ServiceCall call, $0.Server_Query request);
  $async.Future<$0.Server> serverGet($grpc.ServiceCall call, $0.Server request);
  $async.Future<$0.Void> serverStart($grpc.ServiceCall call, $0.Server request);
  $async.Future<$0.Void> serverStop($grpc.ServiceCall call, $0.Server request);
  $async.Future<$0.Void> serverRemove(
      $grpc.ServiceCall call, $0.Server request);
  $async.Stream<$0.Server_Event> serverEvents(
      $grpc.ServiceCall call, $0.Server request);
  $async.Future<$0.Void> contextActionAdd(
      $grpc.ServiceCall call, $0.ContextAction request);
  $async.Future<$0.Void> contextActionRemove(
      $grpc.ServiceCall call, $0.ContextAction request);
  $async.Stream<$0.ContextAction> contextActionEvents(
      $grpc.ServiceCall call, $0.ContextAction request);
  $async.Future<$0.Void> textMessageSend(
      $grpc.ServiceCall call, $0.TextMessage request);
  $async.Stream<$0.TextMessage_Filter> textMessageFilter(
      $grpc.ServiceCall call, $async.Stream<$0.TextMessage_Filter> request);
  $async.Future<$0.Log_List> logQuery(
      $grpc.ServiceCall call, $0.Log_Query request);
  $async.Future<$0.Config> configGet($grpc.ServiceCall call, $0.Server request);
  $async.Future<$0.Config_Field> configGetField(
      $grpc.ServiceCall call, $0.Config_Field request);
  $async.Future<$0.Void> configSetField(
      $grpc.ServiceCall call, $0.Config_Field request);
  $async.Future<$0.Config> configGetDefault(
      $grpc.ServiceCall call, $0.Void request);
  $async.Future<$0.Channel_List> channelQuery(
      $grpc.ServiceCall call, $0.Channel_Query request);
  $async.Future<$0.Channel> channelGet(
      $grpc.ServiceCall call, $0.Channel request);
  $async.Future<$0.Channel> channelAdd(
      $grpc.ServiceCall call, $0.Channel request);
  $async.Future<$0.Void> channelRemove(
      $grpc.ServiceCall call, $0.Channel request);
  $async.Future<$0.Channel> channelUpdate(
      $grpc.ServiceCall call, $0.Channel request);
  $async.Future<$0.User_List> userQuery(
      $grpc.ServiceCall call, $0.User_Query request);
  $async.Future<$0.User> userGet($grpc.ServiceCall call, $0.User request);
  $async.Future<$0.User> userUpdate($grpc.ServiceCall call, $0.User request);
  $async.Future<$0.Void> userKick($grpc.ServiceCall call, $0.User_Kick request);
  $async.Future<$0.Tree> treeQuery(
      $grpc.ServiceCall call, $0.Tree_Query request);
  $async.Future<$0.Ban_List> bansGet(
      $grpc.ServiceCall call, $0.Ban_Query request);
  $async.Future<$0.Void> bansSet($grpc.ServiceCall call, $0.Ban_List request);
  $async.Future<$0.ACL_List> aCLGet($grpc.ServiceCall call, $0.Channel request);
  $async.Future<$0.Void> aCLSet($grpc.ServiceCall call, $0.ACL_List request);
  $async.Future<$0.ACL> aCLGetEffectivePermissions(
      $grpc.ServiceCall call, $0.ACL_Query request);
  $async.Future<$0.Void> aCLAddTemporaryGroup(
      $grpc.ServiceCall call, $0.ACL_TemporaryGroup request);
  $async.Future<$0.Void> aCLRemoveTemporaryGroup(
      $grpc.ServiceCall call, $0.ACL_TemporaryGroup request);
  $async.Stream<$0.Authenticator_Request> authenticatorStream(
      $grpc.ServiceCall call, $async.Stream<$0.Authenticator_Response> request);
  $async.Future<$0.DatabaseUser_List> databaseUserQuery(
      $grpc.ServiceCall call, $0.DatabaseUser_Query request);
  $async.Future<$0.DatabaseUser> databaseUserGet(
      $grpc.ServiceCall call, $0.DatabaseUser request);
  $async.Future<$0.Void> databaseUserUpdate(
      $grpc.ServiceCall call, $0.DatabaseUser request);
  $async.Future<$0.DatabaseUser> databaseUserRegister(
      $grpc.ServiceCall call, $0.DatabaseUser request);
  $async.Future<$0.Void> databaseUserDeregister(
      $grpc.ServiceCall call, $0.DatabaseUser request);
  $async.Future<$0.DatabaseUser> databaseUserVerify(
      $grpc.ServiceCall call, $0.DatabaseUser_Verify request);
  $async.Future<$0.Void> redirectWhisperGroupAdd(
      $grpc.ServiceCall call, $0.RedirectWhisperGroup request);
  $async.Future<$0.Void> redirectWhisperGroupRemove(
      $grpc.ServiceCall call, $0.RedirectWhisperGroup request);
}
