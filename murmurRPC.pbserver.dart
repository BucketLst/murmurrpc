///
//  Generated code. Do not modify.
//  source: murmurRPC/murmurRPC.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

import 'dart:async' as $async;

import 'package:protobuf/protobuf.dart' as $pb;

import 'dart:core' as $core;
import 'murmurRPC.pb.dart' as $0;
import 'murmurRPC.pbjson.dart';

export 'murmurRPC.pb.dart';

abstract class V1ServiceBase extends $pb.GeneratedService {
  $async.Future<$0.Uptime> getUptime($pb.ServerContext ctx, $0.Void request);
  $async.Future<$0.Version> getVersion($pb.ServerContext ctx, $0.Void request);
  $async.Future<$0.Event> events($pb.ServerContext ctx, $0.Void request);
  $async.Future<$0.Server> serverCreate($pb.ServerContext ctx, $0.Void request);
  $async.Future<$0.Server_List> serverQuery($pb.ServerContext ctx, $0.Server_Query request);
  $async.Future<$0.Server> serverGet($pb.ServerContext ctx, $0.Server request);
  $async.Future<$0.Void> serverStart($pb.ServerContext ctx, $0.Server request);
  $async.Future<$0.Void> serverStop($pb.ServerContext ctx, $0.Server request);
  $async.Future<$0.Void> serverRemove($pb.ServerContext ctx, $0.Server request);
  $async.Future<$0.Server_Event> serverEvents($pb.ServerContext ctx, $0.Server request);
  $async.Future<$0.Void> contextActionAdd($pb.ServerContext ctx, $0.ContextAction request);
  $async.Future<$0.Void> contextActionRemove($pb.ServerContext ctx, $0.ContextAction request);
  $async.Future<$0.ContextAction> contextActionEvents($pb.ServerContext ctx, $0.ContextAction request);
  $async.Future<$0.Void> textMessageSend($pb.ServerContext ctx, $0.TextMessage request);
  $async.Future<$0.TextMessage_Filter> textMessageFilter($pb.ServerContext ctx, $0.TextMessage_Filter request);
  $async.Future<$0.Log_List> logQuery($pb.ServerContext ctx, $0.Log_Query request);
  $async.Future<$0.Config> configGet($pb.ServerContext ctx, $0.Server request);
  $async.Future<$0.Config_Field> configGetField($pb.ServerContext ctx, $0.Config_Field request);
  $async.Future<$0.Void> configSetField($pb.ServerContext ctx, $0.Config_Field request);
  $async.Future<$0.Config> configGetDefault($pb.ServerContext ctx, $0.Void request);
  $async.Future<$0.Channel_List> channelQuery($pb.ServerContext ctx, $0.Channel_Query request);
  $async.Future<$0.Channel> channelGet($pb.ServerContext ctx, $0.Channel request);
  $async.Future<$0.Channel> channelAdd($pb.ServerContext ctx, $0.Channel request);
  $async.Future<$0.Void> channelRemove($pb.ServerContext ctx, $0.Channel request);
  $async.Future<$0.Channel> channelUpdate($pb.ServerContext ctx, $0.Channel request);
  $async.Future<$0.User_List> userQuery($pb.ServerContext ctx, $0.User_Query request);
  $async.Future<$0.User> userGet($pb.ServerContext ctx, $0.User request);
  $async.Future<$0.User> userUpdate($pb.ServerContext ctx, $0.User request);
  $async.Future<$0.Void> userKick($pb.ServerContext ctx, $0.User_Kick request);
  $async.Future<$0.Tree> treeQuery($pb.ServerContext ctx, $0.Tree_Query request);
  $async.Future<$0.Ban_List> bansGet($pb.ServerContext ctx, $0.Ban_Query request);
  $async.Future<$0.Void> bansSet($pb.ServerContext ctx, $0.Ban_List request);
  $async.Future<$0.ACL_List> aCLGet($pb.ServerContext ctx, $0.Channel request);
  $async.Future<$0.Void> aCLSet($pb.ServerContext ctx, $0.ACL_List request);
  $async.Future<$0.ACL> aCLGetEffectivePermissions($pb.ServerContext ctx, $0.ACL_Query request);
  $async.Future<$0.Void> aCLAddTemporaryGroup($pb.ServerContext ctx, $0.ACL_TemporaryGroup request);
  $async.Future<$0.Void> aCLRemoveTemporaryGroup($pb.ServerContext ctx, $0.ACL_TemporaryGroup request);
  $async.Future<$0.Authenticator_Request> authenticatorStream($pb.ServerContext ctx, $0.Authenticator_Response request);
  $async.Future<$0.DatabaseUser_List> databaseUserQuery($pb.ServerContext ctx, $0.DatabaseUser_Query request);
  $async.Future<$0.DatabaseUser> databaseUserGet($pb.ServerContext ctx, $0.DatabaseUser request);
  $async.Future<$0.Void> databaseUserUpdate($pb.ServerContext ctx, $0.DatabaseUser request);
  $async.Future<$0.DatabaseUser> databaseUserRegister($pb.ServerContext ctx, $0.DatabaseUser request);
  $async.Future<$0.Void> databaseUserDeregister($pb.ServerContext ctx, $0.DatabaseUser request);
  $async.Future<$0.DatabaseUser> databaseUserVerify($pb.ServerContext ctx, $0.DatabaseUser_Verify request);
  $async.Future<$0.Void> redirectWhisperGroupAdd($pb.ServerContext ctx, $0.RedirectWhisperGroup request);
  $async.Future<$0.Void> redirectWhisperGroupRemove($pb.ServerContext ctx, $0.RedirectWhisperGroup request);

  $pb.GeneratedMessage createRequest($core.String method) {
    switch (method) {
      case 'GetUptime': return $0.Void();
      case 'GetVersion': return $0.Void();
      case 'Events': return $0.Void();
      case 'ServerCreate': return $0.Void();
      case 'ServerQuery': return $0.Server_Query();
      case 'ServerGet': return $0.Server();
      case 'ServerStart': return $0.Server();
      case 'ServerStop': return $0.Server();
      case 'ServerRemove': return $0.Server();
      case 'ServerEvents': return $0.Server();
      case 'ContextActionAdd': return $0.ContextAction();
      case 'ContextActionRemove': return $0.ContextAction();
      case 'ContextActionEvents': return $0.ContextAction();
      case 'TextMessageSend': return $0.TextMessage();
      case 'TextMessageFilter': return $0.TextMessage_Filter();
      case 'LogQuery': return $0.Log_Query();
      case 'ConfigGet': return $0.Server();
      case 'ConfigGetField': return $0.Config_Field();
      case 'ConfigSetField': return $0.Config_Field();
      case 'ConfigGetDefault': return $0.Void();
      case 'ChannelQuery': return $0.Channel_Query();
      case 'ChannelGet': return $0.Channel();
      case 'ChannelAdd': return $0.Channel();
      case 'ChannelRemove': return $0.Channel();
      case 'ChannelUpdate': return $0.Channel();
      case 'UserQuery': return $0.User_Query();
      case 'UserGet': return $0.User();
      case 'UserUpdate': return $0.User();
      case 'UserKick': return $0.User_Kick();
      case 'TreeQuery': return $0.Tree_Query();
      case 'BansGet': return $0.Ban_Query();
      case 'BansSet': return $0.Ban_List();
      case 'ACLGet': return $0.Channel();
      case 'ACLSet': return $0.ACL_List();
      case 'ACLGetEffectivePermissions': return $0.ACL_Query();
      case 'ACLAddTemporaryGroup': return $0.ACL_TemporaryGroup();
      case 'ACLRemoveTemporaryGroup': return $0.ACL_TemporaryGroup();
      case 'AuthenticatorStream': return $0.Authenticator_Response();
      case 'DatabaseUserQuery': return $0.DatabaseUser_Query();
      case 'DatabaseUserGet': return $0.DatabaseUser();
      case 'DatabaseUserUpdate': return $0.DatabaseUser();
      case 'DatabaseUserRegister': return $0.DatabaseUser();
      case 'DatabaseUserDeregister': return $0.DatabaseUser();
      case 'DatabaseUserVerify': return $0.DatabaseUser_Verify();
      case 'RedirectWhisperGroupAdd': return $0.RedirectWhisperGroup();
      case 'RedirectWhisperGroupRemove': return $0.RedirectWhisperGroup();
      default: throw $core.ArgumentError('Unknown method: $method');
    }
  }

  $async.Future<$pb.GeneratedMessage> handleCall($pb.ServerContext ctx, $core.String method, $pb.GeneratedMessage request) {
    switch (method) {
      case 'GetUptime': return this.getUptime(ctx, request);
      case 'GetVersion': return this.getVersion(ctx, request);
      case 'Events': return this.events(ctx, request);
      case 'ServerCreate': return this.serverCreate(ctx, request);
      case 'ServerQuery': return this.serverQuery(ctx, request);
      case 'ServerGet': return this.serverGet(ctx, request);
      case 'ServerStart': return this.serverStart(ctx, request);
      case 'ServerStop': return this.serverStop(ctx, request);
      case 'ServerRemove': return this.serverRemove(ctx, request);
      case 'ServerEvents': return this.serverEvents(ctx, request);
      case 'ContextActionAdd': return this.contextActionAdd(ctx, request);
      case 'ContextActionRemove': return this.contextActionRemove(ctx, request);
      case 'ContextActionEvents': return this.contextActionEvents(ctx, request);
      case 'TextMessageSend': return this.textMessageSend(ctx, request);
      case 'TextMessageFilter': return this.textMessageFilter(ctx, request);
      case 'LogQuery': return this.logQuery(ctx, request);
      case 'ConfigGet': return this.configGet(ctx, request);
      case 'ConfigGetField': return this.configGetField(ctx, request);
      case 'ConfigSetField': return this.configSetField(ctx, request);
      case 'ConfigGetDefault': return this.configGetDefault(ctx, request);
      case 'ChannelQuery': return this.channelQuery(ctx, request);
      case 'ChannelGet': return this.channelGet(ctx, request);
      case 'ChannelAdd': return this.channelAdd(ctx, request);
      case 'ChannelRemove': return this.channelRemove(ctx, request);
      case 'ChannelUpdate': return this.channelUpdate(ctx, request);
      case 'UserQuery': return this.userQuery(ctx, request);
      case 'UserGet': return this.userGet(ctx, request);
      case 'UserUpdate': return this.userUpdate(ctx, request);
      case 'UserKick': return this.userKick(ctx, request);
      case 'TreeQuery': return this.treeQuery(ctx, request);
      case 'BansGet': return this.bansGet(ctx, request);
      case 'BansSet': return this.bansSet(ctx, request);
      case 'ACLGet': return this.aCLGet(ctx, request);
      case 'ACLSet': return this.aCLSet(ctx, request);
      case 'ACLGetEffectivePermissions': return this.aCLGetEffectivePermissions(ctx, request);
      case 'ACLAddTemporaryGroup': return this.aCLAddTemporaryGroup(ctx, request);
      case 'ACLRemoveTemporaryGroup': return this.aCLRemoveTemporaryGroup(ctx, request);
      case 'AuthenticatorStream': return this.authenticatorStream(ctx, request);
      case 'DatabaseUserQuery': return this.databaseUserQuery(ctx, request);
      case 'DatabaseUserGet': return this.databaseUserGet(ctx, request);
      case 'DatabaseUserUpdate': return this.databaseUserUpdate(ctx, request);
      case 'DatabaseUserRegister': return this.databaseUserRegister(ctx, request);
      case 'DatabaseUserDeregister': return this.databaseUserDeregister(ctx, request);
      case 'DatabaseUserVerify': return this.databaseUserVerify(ctx, request);
      case 'RedirectWhisperGroupAdd': return this.redirectWhisperGroupAdd(ctx, request);
      case 'RedirectWhisperGroupRemove': return this.redirectWhisperGroupRemove(ctx, request);
      default: throw $core.ArgumentError('Unknown method: $method');
    }
  }

  $core.Map<$core.String, $core.dynamic> get $json => V1ServiceBase$json;
  $core.Map<$core.String, $core.Map<$core.String, $core.dynamic>> get $messageJson => V1ServiceBase$messageJson;
}

