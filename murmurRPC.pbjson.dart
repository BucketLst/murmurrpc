///
//  Generated code. Do not modify.
//  source: murmurRPC.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

const Void$json = const {
  '1': 'Void',
};

const Version$json = const {
  '1': 'Version',
  '2': const [
    const {'1': 'version', '3': 1, '4': 1, '5': 13, '10': 'version'},
    const {'1': 'release', '3': 2, '4': 1, '5': 9, '10': 'release'},
    const {'1': 'os', '3': 3, '4': 1, '5': 9, '10': 'os'},
    const {'1': 'os_version', '3': 4, '4': 1, '5': 9, '10': 'osVersion'},
  ],
};

const Uptime$json = const {
  '1': 'Uptime',
  '2': const [
    const {'1': 'secs', '3': 1, '4': 1, '5': 4, '10': 'secs'},
  ],
};

const Server$json = const {
  '1': 'Server',
  '2': const [
    const {'1': 'id', '3': 1, '4': 2, '5': 13, '10': 'id'},
    const {'1': 'running', '3': 2, '4': 1, '5': 8, '10': 'running'},
    const {'1': 'uptime', '3': 3, '4': 1, '5': 11, '6': '.MurmurRPC.Uptime', '10': 'uptime'},
  ],
  '3': const [Server_Event$json, Server_Query$json, Server_List$json],
};

const Server_Event$json = const {
  '1': 'Event',
  '2': const [
    const {'1': 'server', '3': 1, '4': 1, '5': 11, '6': '.MurmurRPC.Server', '10': 'server'},
    const {'1': 'type', '3': 2, '4': 1, '5': 14, '6': '.MurmurRPC.Server.Event.Type', '10': 'type'},
    const {'1': 'user', '3': 3, '4': 1, '5': 11, '6': '.MurmurRPC.User', '10': 'user'},
    const {'1': 'message', '3': 4, '4': 1, '5': 11, '6': '.MurmurRPC.TextMessage', '10': 'message'},
    const {'1': 'channel', '3': 5, '4': 1, '5': 11, '6': '.MurmurRPC.Channel', '10': 'channel'},
  ],
  '4': const [Server_Event_Type$json],
};

const Server_Event_Type$json = const {
  '1': 'Type',
  '2': const [
    const {'1': 'UserConnected', '2': 0},
    const {'1': 'UserDisconnected', '2': 1},
    const {'1': 'UserStateChanged', '2': 2},
    const {'1': 'UserTextMessage', '2': 3},
    const {'1': 'ChannelCreated', '2': 4},
    const {'1': 'ChannelRemoved', '2': 5},
    const {'1': 'ChannelStateChanged', '2': 6},
  ],
};

const Server_Query$json = const {
  '1': 'Query',
};

const Server_List$json = const {
  '1': 'List',
  '2': const [
    const {'1': 'servers', '3': 1, '4': 3, '5': 11, '6': '.MurmurRPC.Server', '10': 'servers'},
  ],
};

const Event$json = const {
  '1': 'Event',
  '2': const [
    const {'1': 'server', '3': 1, '4': 1, '5': 11, '6': '.MurmurRPC.Server', '10': 'server'},
    const {'1': 'type', '3': 2, '4': 1, '5': 14, '6': '.MurmurRPC.Event.Type', '10': 'type'},
  ],
  '4': const [Event_Type$json],
};

const Event_Type$json = const {
  '1': 'Type',
  '2': const [
    const {'1': 'ServerStopped', '2': 0},
    const {'1': 'ServerStarted', '2': 1},
  ],
};

const ContextAction$json = const {
  '1': 'ContextAction',
  '2': const [
    const {'1': 'server', '3': 1, '4': 1, '5': 11, '6': '.MurmurRPC.Server', '10': 'server'},
    const {'1': 'context', '3': 2, '4': 1, '5': 13, '10': 'context'},
    const {'1': 'action', '3': 3, '4': 1, '5': 9, '10': 'action'},
    const {'1': 'text', '3': 4, '4': 1, '5': 9, '10': 'text'},
    const {'1': 'actor', '3': 5, '4': 1, '5': 11, '6': '.MurmurRPC.User', '10': 'actor'},
    const {'1': 'user', '3': 6, '4': 1, '5': 11, '6': '.MurmurRPC.User', '10': 'user'},
    const {'1': 'channel', '3': 7, '4': 1, '5': 11, '6': '.MurmurRPC.Channel', '10': 'channel'},
  ],
  '4': const [ContextAction_Context$json],
};

const ContextAction_Context$json = const {
  '1': 'Context',
  '2': const [
    const {'1': 'Server', '2': 1},
    const {'1': 'Channel', '2': 2},
    const {'1': 'User', '2': 4},
  ],
};

const TextMessage$json = const {
  '1': 'TextMessage',
  '2': const [
    const {'1': 'server', '3': 1, '4': 1, '5': 11, '6': '.MurmurRPC.Server', '10': 'server'},
    const {'1': 'actor', '3': 2, '4': 1, '5': 11, '6': '.MurmurRPC.User', '10': 'actor'},
    const {'1': 'users', '3': 3, '4': 3, '5': 11, '6': '.MurmurRPC.User', '10': 'users'},
    const {'1': 'channels', '3': 4, '4': 3, '5': 11, '6': '.MurmurRPC.Channel', '10': 'channels'},
    const {'1': 'trees', '3': 5, '4': 3, '5': 11, '6': '.MurmurRPC.Channel', '10': 'trees'},
    const {'1': 'text', '3': 6, '4': 1, '5': 9, '10': 'text'},
  ],
  '3': const [TextMessage_Filter$json],
};

const TextMessage_Filter$json = const {
  '1': 'Filter',
  '2': const [
    const {'1': 'server', '3': 1, '4': 1, '5': 11, '6': '.MurmurRPC.Server', '10': 'server'},
    const {'1': 'action', '3': 2, '4': 1, '5': 14, '6': '.MurmurRPC.TextMessage.Filter.Action', '10': 'action'},
    const {'1': 'message', '3': 3, '4': 1, '5': 11, '6': '.MurmurRPC.TextMessage', '10': 'message'},
  ],
  '4': const [TextMessage_Filter_Action$json],
};

const TextMessage_Filter_Action$json = const {
  '1': 'Action',
  '2': const [
    const {'1': 'Accept', '2': 0},
    const {'1': 'Reject', '2': 1},
    const {'1': 'Drop', '2': 2},
  ],
};

const Log$json = const {
  '1': 'Log',
  '2': const [
    const {'1': 'server', '3': 1, '4': 1, '5': 11, '6': '.MurmurRPC.Server', '10': 'server'},
    const {'1': 'timestamp', '3': 2, '4': 1, '5': 3, '10': 'timestamp'},
    const {'1': 'text', '3': 3, '4': 1, '5': 9, '10': 'text'},
  ],
  '3': const [Log_Query$json, Log_List$json],
};

const Log_Query$json = const {
  '1': 'Query',
  '2': const [
    const {'1': 'server', '3': 1, '4': 1, '5': 11, '6': '.MurmurRPC.Server', '10': 'server'},
    const {'1': 'min', '3': 2, '4': 1, '5': 13, '10': 'min'},
    const {'1': 'max', '3': 3, '4': 1, '5': 13, '10': 'max'},
  ],
};

const Log_List$json = const {
  '1': 'List',
  '2': const [
    const {'1': 'server', '3': 1, '4': 1, '5': 11, '6': '.MurmurRPC.Server', '10': 'server'},
    const {'1': 'total', '3': 2, '4': 1, '5': 13, '10': 'total'},
    const {'1': 'min', '3': 3, '4': 1, '5': 13, '10': 'min'},
    const {'1': 'max', '3': 4, '4': 1, '5': 13, '10': 'max'},
    const {'1': 'entries', '3': 5, '4': 3, '5': 11, '6': '.MurmurRPC.Log', '10': 'entries'},
  ],
};

const Config$json = const {
  '1': 'Config',
  '2': const [
    const {'1': 'server', '3': 1, '4': 1, '5': 11, '6': '.MurmurRPC.Server', '10': 'server'},
    const {'1': 'fields', '3': 2, '4': 3, '5': 11, '6': '.MurmurRPC.Config.FieldsEntry', '10': 'fields'},
  ],
  '3': const [Config_FieldsEntry$json, Config_Field$json],
};

const Config_FieldsEntry$json = const {
  '1': 'FieldsEntry',
  '2': const [
    const {'1': 'key', '3': 1, '4': 1, '5': 9, '10': 'key'},
    const {'1': 'value', '3': 2, '4': 1, '5': 9, '10': 'value'},
  ],
  '7': const {'7': true},
};

const Config_Field$json = const {
  '1': 'Field',
  '2': const [
    const {'1': 'server', '3': 1, '4': 1, '5': 11, '6': '.MurmurRPC.Server', '10': 'server'},
    const {'1': 'key', '3': 2, '4': 1, '5': 9, '10': 'key'},
    const {'1': 'value', '3': 3, '4': 1, '5': 9, '10': 'value'},
  ],
};

const Channel$json = const {
  '1': 'Channel',
  '2': const [
    const {'1': 'server', '3': 1, '4': 1, '5': 11, '6': '.MurmurRPC.Server', '10': 'server'},
    const {'1': 'id', '3': 2, '4': 1, '5': 13, '10': 'id'},
    const {'1': 'name', '3': 3, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'parent', '3': 4, '4': 1, '5': 11, '6': '.MurmurRPC.Channel', '10': 'parent'},
    const {'1': 'links', '3': 5, '4': 3, '5': 11, '6': '.MurmurRPC.Channel', '10': 'links'},
    const {'1': 'description', '3': 6, '4': 1, '5': 9, '10': 'description'},
    const {'1': 'temporary', '3': 7, '4': 1, '5': 8, '10': 'temporary'},
    const {'1': 'position', '3': 8, '4': 1, '5': 5, '10': 'position'},
  ],
  '3': const [Channel_Query$json, Channel_List$json],
};

const Channel_Query$json = const {
  '1': 'Query',
  '2': const [
    const {'1': 'server', '3': 1, '4': 1, '5': 11, '6': '.MurmurRPC.Server', '10': 'server'},
  ],
};

const Channel_List$json = const {
  '1': 'List',
  '2': const [
    const {'1': 'server', '3': 1, '4': 1, '5': 11, '6': '.MurmurRPC.Server', '10': 'server'},
    const {'1': 'channels', '3': 2, '4': 3, '5': 11, '6': '.MurmurRPC.Channel', '10': 'channels'},
  ],
};

const User$json = const {
  '1': 'User',
  '2': const [
    const {'1': 'server', '3': 1, '4': 1, '5': 11, '6': '.MurmurRPC.Server', '10': 'server'},
    const {'1': 'session', '3': 2, '4': 1, '5': 13, '10': 'session'},
    const {'1': 'id', '3': 3, '4': 1, '5': 13, '10': 'id'},
    const {'1': 'name', '3': 4, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'mute', '3': 5, '4': 1, '5': 8, '10': 'mute'},
    const {'1': 'deaf', '3': 6, '4': 1, '5': 8, '10': 'deaf'},
    const {'1': 'suppress', '3': 7, '4': 1, '5': 8, '10': 'suppress'},
    const {'1': 'priority_speaker', '3': 8, '4': 1, '5': 8, '10': 'prioritySpeaker'},
    const {'1': 'self_mute', '3': 9, '4': 1, '5': 8, '10': 'selfMute'},
    const {'1': 'self_deaf', '3': 10, '4': 1, '5': 8, '10': 'selfDeaf'},
    const {'1': 'recording', '3': 11, '4': 1, '5': 8, '10': 'recording'},
    const {'1': 'channel', '3': 12, '4': 1, '5': 11, '6': '.MurmurRPC.Channel', '10': 'channel'},
    const {'1': 'online_secs', '3': 13, '4': 1, '5': 13, '10': 'onlineSecs'},
    const {'1': 'idle_secs', '3': 14, '4': 1, '5': 13, '10': 'idleSecs'},
    const {'1': 'bytes_per_sec', '3': 15, '4': 1, '5': 13, '10': 'bytesPerSec'},
    const {'1': 'version', '3': 16, '4': 1, '5': 11, '6': '.MurmurRPC.Version', '10': 'version'},
    const {'1': 'plugin_context', '3': 17, '4': 1, '5': 12, '10': 'pluginContext'},
    const {'1': 'plugin_identity', '3': 18, '4': 1, '5': 9, '10': 'pluginIdentity'},
    const {'1': 'comment', '3': 19, '4': 1, '5': 9, '10': 'comment'},
    const {'1': 'texture', '3': 20, '4': 1, '5': 12, '10': 'texture'},
    const {'1': 'address', '3': 21, '4': 1, '5': 12, '10': 'address'},
    const {'1': 'tcp_only', '3': 22, '4': 1, '5': 8, '10': 'tcpOnly'},
    const {'1': 'udp_ping_msecs', '3': 23, '4': 1, '5': 2, '10': 'udpPingMsecs'},
    const {'1': 'tcp_ping_msecs', '3': 24, '4': 1, '5': 2, '10': 'tcpPingMsecs'},
  ],
  '3': const [User_Query$json, User_List$json, User_Kick$json],
};

const User_Query$json = const {
  '1': 'Query',
  '2': const [
    const {'1': 'server', '3': 1, '4': 1, '5': 11, '6': '.MurmurRPC.Server', '10': 'server'},
  ],
};

const User_List$json = const {
  '1': 'List',
  '2': const [
    const {'1': 'server', '3': 1, '4': 1, '5': 11, '6': '.MurmurRPC.Server', '10': 'server'},
    const {'1': 'users', '3': 2, '4': 3, '5': 11, '6': '.MurmurRPC.User', '10': 'users'},
  ],
};

const User_Kick$json = const {
  '1': 'Kick',
  '2': const [
    const {'1': 'server', '3': 1, '4': 1, '5': 11, '6': '.MurmurRPC.Server', '10': 'server'},
    const {'1': 'user', '3': 2, '4': 1, '5': 11, '6': '.MurmurRPC.User', '10': 'user'},
    const {'1': 'actor', '3': 3, '4': 1, '5': 11, '6': '.MurmurRPC.User', '10': 'actor'},
    const {'1': 'reason', '3': 4, '4': 1, '5': 9, '10': 'reason'},
  ],
};

const Tree$json = const {
  '1': 'Tree',
  '2': const [
    const {'1': 'server', '3': 1, '4': 1, '5': 11, '6': '.MurmurRPC.Server', '10': 'server'},
    const {'1': 'channel', '3': 2, '4': 1, '5': 11, '6': '.MurmurRPC.Channel', '10': 'channel'},
    const {'1': 'children', '3': 3, '4': 3, '5': 11, '6': '.MurmurRPC.Tree', '10': 'children'},
    const {'1': 'users', '3': 4, '4': 3, '5': 11, '6': '.MurmurRPC.User', '10': 'users'},
  ],
  '3': const [Tree_Query$json],
};

const Tree_Query$json = const {
  '1': 'Query',
  '2': const [
    const {'1': 'server', '3': 1, '4': 1, '5': 11, '6': '.MurmurRPC.Server', '10': 'server'},
  ],
};

const Ban$json = const {
  '1': 'Ban',
  '2': const [
    const {'1': 'server', '3': 1, '4': 1, '5': 11, '6': '.MurmurRPC.Server', '10': 'server'},
    const {'1': 'address', '3': 2, '4': 1, '5': 12, '10': 'address'},
    const {'1': 'bits', '3': 3, '4': 1, '5': 13, '10': 'bits'},
    const {'1': 'name', '3': 4, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'hash', '3': 5, '4': 1, '5': 9, '10': 'hash'},
    const {'1': 'reason', '3': 6, '4': 1, '5': 9, '10': 'reason'},
    const {'1': 'start', '3': 7, '4': 1, '5': 3, '10': 'start'},
    const {'1': 'duration_secs', '3': 8, '4': 1, '5': 3, '10': 'durationSecs'},
  ],
  '3': const [Ban_Query$json, Ban_List$json],
};

const Ban_Query$json = const {
  '1': 'Query',
  '2': const [
    const {'1': 'server', '3': 1, '4': 1, '5': 11, '6': '.MurmurRPC.Server', '10': 'server'},
  ],
};

const Ban_List$json = const {
  '1': 'List',
  '2': const [
    const {'1': 'server', '3': 1, '4': 1, '5': 11, '6': '.MurmurRPC.Server', '10': 'server'},
    const {'1': 'bans', '3': 2, '4': 3, '5': 11, '6': '.MurmurRPC.Ban', '10': 'bans'},
  ],
};

const ACL$json = const {
  '1': 'ACL',
  '2': const [
    const {'1': 'apply_here', '3': 3, '4': 1, '5': 8, '10': 'applyHere'},
    const {'1': 'apply_subs', '3': 4, '4': 1, '5': 8, '10': 'applySubs'},
    const {'1': 'inherited', '3': 5, '4': 1, '5': 8, '10': 'inherited'},
    const {'1': 'user', '3': 6, '4': 1, '5': 11, '6': '.MurmurRPC.DatabaseUser', '10': 'user'},
    const {'1': 'group', '3': 7, '4': 1, '5': 11, '6': '.MurmurRPC.ACL.Group', '10': 'group'},
    const {'1': 'allow', '3': 8, '4': 1, '5': 13, '10': 'allow'},
    const {'1': 'deny', '3': 9, '4': 1, '5': 13, '10': 'deny'},
  ],
  '3': const [ACL_Group$json, ACL_Query$json, ACL_List$json, ACL_TemporaryGroup$json],
  '4': const [ACL_Permission$json],
};

const ACL_Group$json = const {
  '1': 'Group',
  '2': const [
    const {'1': 'name', '3': 1, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'inherited', '3': 2, '4': 1, '5': 8, '10': 'inherited'},
    const {'1': 'inherit', '3': 3, '4': 1, '5': 8, '10': 'inherit'},
    const {'1': 'inheritable', '3': 4, '4': 1, '5': 8, '10': 'inheritable'},
    const {'1': 'users_add', '3': 5, '4': 3, '5': 11, '6': '.MurmurRPC.DatabaseUser', '10': 'usersAdd'},
    const {'1': 'users_remove', '3': 6, '4': 3, '5': 11, '6': '.MurmurRPC.DatabaseUser', '10': 'usersRemove'},
    const {'1': 'users', '3': 7, '4': 3, '5': 11, '6': '.MurmurRPC.DatabaseUser', '10': 'users'},
  ],
};

const ACL_Query$json = const {
  '1': 'Query',
  '2': const [
    const {'1': 'server', '3': 1, '4': 1, '5': 11, '6': '.MurmurRPC.Server', '10': 'server'},
    const {'1': 'user', '3': 2, '4': 1, '5': 11, '6': '.MurmurRPC.User', '10': 'user'},
    const {'1': 'channel', '3': 3, '4': 1, '5': 11, '6': '.MurmurRPC.Channel', '10': 'channel'},
  ],
};

const ACL_List$json = const {
  '1': 'List',
  '2': const [
    const {'1': 'server', '3': 1, '4': 1, '5': 11, '6': '.MurmurRPC.Server', '10': 'server'},
    const {'1': 'channel', '3': 2, '4': 1, '5': 11, '6': '.MurmurRPC.Channel', '10': 'channel'},
    const {'1': 'acls', '3': 3, '4': 3, '5': 11, '6': '.MurmurRPC.ACL', '10': 'acls'},
    const {'1': 'groups', '3': 4, '4': 3, '5': 11, '6': '.MurmurRPC.ACL.Group', '10': 'groups'},
    const {'1': 'inherit', '3': 5, '4': 1, '5': 8, '10': 'inherit'},
  ],
};

const ACL_TemporaryGroup$json = const {
  '1': 'TemporaryGroup',
  '2': const [
    const {'1': 'server', '3': 1, '4': 1, '5': 11, '6': '.MurmurRPC.Server', '10': 'server'},
    const {'1': 'channel', '3': 2, '4': 1, '5': 11, '6': '.MurmurRPC.Channel', '10': 'channel'},
    const {'1': 'user', '3': 3, '4': 1, '5': 11, '6': '.MurmurRPC.User', '10': 'user'},
    const {'1': 'name', '3': 4, '4': 1, '5': 9, '10': 'name'},
  ],
};

const ACL_Permission$json = const {
  '1': 'Permission',
  '2': const [
    const {'1': 'None', '2': 0},
    const {'1': 'Write', '2': 1},
    const {'1': 'Traverse', '2': 2},
    const {'1': 'Enter', '2': 4},
    const {'1': 'Speak', '2': 8},
    const {'1': 'Whisper', '2': 256},
    const {'1': 'MuteDeafen', '2': 16},
    const {'1': 'Move', '2': 32},
    const {'1': 'MakeChannel', '2': 64},
    const {'1': 'MakeTemporaryChannel', '2': 1024},
    const {'1': 'LinkChannel', '2': 128},
    const {'1': 'TextMessage', '2': 512},
    const {'1': 'Kick', '2': 65536},
    const {'1': 'Ban', '2': 131072},
    const {'1': 'Register', '2': 262144},
    const {'1': 'RegisterSelf', '2': 524288},
  ],
};

const Authenticator$json = const {
  '1': 'Authenticator',
  '3': const [Authenticator_Request$json, Authenticator_Response$json],
};

const Authenticator_Request$json = const {
  '1': 'Request',
  '2': const [
    const {'1': 'authenticate', '3': 1, '4': 1, '5': 11, '6': '.MurmurRPC.Authenticator.Request.Authenticate', '10': 'authenticate'},
    const {'1': 'find', '3': 2, '4': 1, '5': 11, '6': '.MurmurRPC.Authenticator.Request.Find', '10': 'find'},
    const {'1': 'query', '3': 3, '4': 1, '5': 11, '6': '.MurmurRPC.Authenticator.Request.Query', '10': 'query'},
    const {'1': 'register', '3': 4, '4': 1, '5': 11, '6': '.MurmurRPC.Authenticator.Request.Register', '10': 'register'},
    const {'1': 'deregister', '3': 5, '4': 1, '5': 11, '6': '.MurmurRPC.Authenticator.Request.Deregister', '10': 'deregister'},
    const {'1': 'update', '3': 6, '4': 1, '5': 11, '6': '.MurmurRPC.Authenticator.Request.Update', '10': 'update'},
  ],
  '3': const [Authenticator_Request_Authenticate$json, Authenticator_Request_Find$json, Authenticator_Request_Query$json, Authenticator_Request_Register$json, Authenticator_Request_Deregister$json, Authenticator_Request_Update$json],
};

const Authenticator_Request_Authenticate$json = const {
  '1': 'Authenticate',
  '2': const [
    const {'1': 'name', '3': 1, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'password', '3': 2, '4': 1, '5': 9, '10': 'password'},
    const {'1': 'certificates', '3': 3, '4': 3, '5': 12, '10': 'certificates'},
    const {'1': 'certificate_hash', '3': 4, '4': 1, '5': 9, '10': 'certificateHash'},
    const {'1': 'strong_certificate', '3': 5, '4': 1, '5': 8, '10': 'strongCertificate'},
  ],
};

const Authenticator_Request_Find$json = const {
  '1': 'Find',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 13, '10': 'id'},
    const {'1': 'name', '3': 2, '4': 1, '5': 9, '10': 'name'},
  ],
};

const Authenticator_Request_Query$json = const {
  '1': 'Query',
  '2': const [
    const {'1': 'filter', '3': 1, '4': 1, '5': 9, '10': 'filter'},
  ],
};

const Authenticator_Request_Register$json = const {
  '1': 'Register',
  '2': const [
    const {'1': 'user', '3': 1, '4': 1, '5': 11, '6': '.MurmurRPC.DatabaseUser', '10': 'user'},
  ],
};

const Authenticator_Request_Deregister$json = const {
  '1': 'Deregister',
  '2': const [
    const {'1': 'user', '3': 1, '4': 1, '5': 11, '6': '.MurmurRPC.DatabaseUser', '10': 'user'},
  ],
};

const Authenticator_Request_Update$json = const {
  '1': 'Update',
  '2': const [
    const {'1': 'user', '3': 1, '4': 1, '5': 11, '6': '.MurmurRPC.DatabaseUser', '10': 'user'},
  ],
};

const Authenticator_Response$json = const {
  '1': 'Response',
  '2': const [
    const {'1': 'initialize', '3': 1, '4': 1, '5': 11, '6': '.MurmurRPC.Authenticator.Response.Initialize', '10': 'initialize'},
    const {'1': 'authenticate', '3': 2, '4': 1, '5': 11, '6': '.MurmurRPC.Authenticator.Response.Authenticate', '10': 'authenticate'},
    const {'1': 'find', '3': 3, '4': 1, '5': 11, '6': '.MurmurRPC.Authenticator.Response.Find', '10': 'find'},
    const {'1': 'query', '3': 4, '4': 1, '5': 11, '6': '.MurmurRPC.Authenticator.Response.Query', '10': 'query'},
    const {'1': 'register', '3': 5, '4': 1, '5': 11, '6': '.MurmurRPC.Authenticator.Response.Register', '10': 'register'},
    const {'1': 'deregister', '3': 6, '4': 1, '5': 11, '6': '.MurmurRPC.Authenticator.Response.Deregister', '10': 'deregister'},
    const {'1': 'update', '3': 7, '4': 1, '5': 11, '6': '.MurmurRPC.Authenticator.Response.Update', '10': 'update'},
  ],
  '3': const [Authenticator_Response_Initialize$json, Authenticator_Response_Authenticate$json, Authenticator_Response_Find$json, Authenticator_Response_Query$json, Authenticator_Response_Register$json, Authenticator_Response_Deregister$json, Authenticator_Response_Update$json],
  '4': const [Authenticator_Response_Status$json],
};

const Authenticator_Response_Initialize$json = const {
  '1': 'Initialize',
  '2': const [
    const {'1': 'server', '3': 1, '4': 1, '5': 11, '6': '.MurmurRPC.Server', '10': 'server'},
  ],
};

const Authenticator_Response_Authenticate$json = const {
  '1': 'Authenticate',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 14, '6': '.MurmurRPC.Authenticator.Response.Status', '10': 'status'},
    const {'1': 'id', '3': 2, '4': 1, '5': 13, '10': 'id'},
    const {'1': 'name', '3': 3, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'groups', '3': 4, '4': 3, '5': 11, '6': '.MurmurRPC.ACL.Group', '10': 'groups'},
  ],
};

const Authenticator_Response_Find$json = const {
  '1': 'Find',
  '2': const [
    const {'1': 'user', '3': 1, '4': 1, '5': 11, '6': '.MurmurRPC.DatabaseUser', '10': 'user'},
  ],
};

const Authenticator_Response_Query$json = const {
  '1': 'Query',
  '2': const [
    const {'1': 'users', '3': 1, '4': 3, '5': 11, '6': '.MurmurRPC.DatabaseUser', '10': 'users'},
  ],
};

const Authenticator_Response_Register$json = const {
  '1': 'Register',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 14, '6': '.MurmurRPC.Authenticator.Response.Status', '10': 'status'},
    const {'1': 'user', '3': 2, '4': 1, '5': 11, '6': '.MurmurRPC.DatabaseUser', '10': 'user'},
  ],
};

const Authenticator_Response_Deregister$json = const {
  '1': 'Deregister',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 14, '6': '.MurmurRPC.Authenticator.Response.Status', '10': 'status'},
  ],
};

const Authenticator_Response_Update$json = const {
  '1': 'Update',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 14, '6': '.MurmurRPC.Authenticator.Response.Status', '10': 'status'},
  ],
};

const Authenticator_Response_Status$json = const {
  '1': 'Status',
  '2': const [
    const {'1': 'Fallthrough', '2': 0},
    const {'1': 'Success', '2': 1},
    const {'1': 'Failure', '2': 2},
    const {'1': 'TemporaryFailure', '2': 3},
  ],
};

const DatabaseUser$json = const {
  '1': 'DatabaseUser',
  '2': const [
    const {'1': 'server', '3': 1, '4': 1, '5': 11, '6': '.MurmurRPC.Server', '10': 'server'},
    const {'1': 'id', '3': 2, '4': 1, '5': 13, '10': 'id'},
    const {'1': 'name', '3': 3, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'email', '3': 4, '4': 1, '5': 9, '10': 'email'},
    const {'1': 'comment', '3': 5, '4': 1, '5': 9, '10': 'comment'},
    const {'1': 'hash', '3': 6, '4': 1, '5': 9, '10': 'hash'},
    const {'1': 'password', '3': 7, '4': 1, '5': 9, '10': 'password'},
    const {'1': 'last_active', '3': 8, '4': 1, '5': 9, '10': 'lastActive'},
    const {'1': 'texture', '3': 9, '4': 1, '5': 12, '10': 'texture'},
  ],
  '3': const [DatabaseUser_Query$json, DatabaseUser_List$json, DatabaseUser_Verify$json],
};

const DatabaseUser_Query$json = const {
  '1': 'Query',
  '2': const [
    const {'1': 'server', '3': 1, '4': 1, '5': 11, '6': '.MurmurRPC.Server', '10': 'server'},
    const {'1': 'filter', '3': 2, '4': 1, '5': 9, '10': 'filter'},
  ],
};

const DatabaseUser_List$json = const {
  '1': 'List',
  '2': const [
    const {'1': 'server', '3': 1, '4': 1, '5': 11, '6': '.MurmurRPC.Server', '10': 'server'},
    const {'1': 'users', '3': 2, '4': 3, '5': 11, '6': '.MurmurRPC.DatabaseUser', '10': 'users'},
  ],
};

const DatabaseUser_Verify$json = const {
  '1': 'Verify',
  '2': const [
    const {'1': 'server', '3': 1, '4': 1, '5': 11, '6': '.MurmurRPC.Server', '10': 'server'},
    const {'1': 'name', '3': 2, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'password', '3': 3, '4': 1, '5': 9, '10': 'password'},
  ],
};

const RedirectWhisperGroup$json = const {
  '1': 'RedirectWhisperGroup',
  '2': const [
    const {'1': 'server', '3': 1, '4': 1, '5': 11, '6': '.MurmurRPC.Server', '10': 'server'},
    const {'1': 'user', '3': 2, '4': 1, '5': 11, '6': '.MurmurRPC.User', '10': 'user'},
    const {'1': 'source', '3': 3, '4': 1, '5': 11, '6': '.MurmurRPC.ACL.Group', '10': 'source'},
    const {'1': 'target', '3': 4, '4': 1, '5': 11, '6': '.MurmurRPC.ACL.Group', '10': 'target'},
  ],
};

